package userapi

import (
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"user-service/user"
)

func allUserXMLHandler(w http.ResponseWriter, r *http.Request) {
	users, err := user.All()
	if err != nil {
		http.Error(w, fmt.Sprintf("users: %s", err), http.StatusInternalServerError)
		return
	}

	type Users struct {
		XMLName xml.Name `xml:"users"`
		All     []user.User
	}

	byteUsers, err := xml.Marshal(Users{All: users})
	if err != nil {
		http.Error(w, fmt.Sprintf("users: %s", err), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/xml")
	fmt.Fprintf(w, "%s", byteUsers)
}

func createUserXMLHanlder(w http.ResponseWriter, r *http.Request) {
	b, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, fmt.Sprintf("users: %s", err), http.StatusInternalServerError)
		return
	}

	var u user.User
	err = xml.Unmarshal(b, &u)
	if err != nil {
		http.Error(w, fmt.Sprintf("users: %s", err), http.StatusInternalServerError)
		return
	}

	err = user.Insert(&u)
	if err != nil {
		http.Error(w, fmt.Sprintf("users: %s", err), http.StatusInternalServerError)
		return
	}

	byteUser, err := xml.Marshal(u)
	if err != nil {
		http.Error(w, fmt.Sprintf("users: %s", err), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/xml")
	w.WriteHeader(http.StatusCreated)
	fmt.Fprintf(w, "%s", byteUser)
}

func getUserXMLHandler(id int, w http.ResponseWriter, r *http.Request) {
	u, err := user.FindByID(id)
	if err != nil {
		http.Error(w, fmt.Sprintf("users: %s", err), http.StatusNotFound)
		return
	}

	byteUser, err := xml.Marshal(u)
	if err != nil {
		http.Error(w, fmt.Sprintf("users: %s", err), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/xml")
	fmt.Fprintf(w, "%s", byteUser)
}

func deleteUserXMLHandler(id int, w http.ResponseWriter, r *http.Request) {
	err := user.Delete(&user.User{
		ID: id,
	})
	if err != nil {
		http.Error(w, fmt.Sprintf("users: %s", err), http.StatusInternalServerError)
		return
	}
}

func updateUserXMLHandler(id int, w http.ResponseWriter, r *http.Request) {
	u, err := user.FindByID(id)
	if err != nil {
		http.Error(w, fmt.Sprintf("users: %s", err), http.StatusNotFound)
		return
	}

	b, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, fmt.Sprintf("users: %s", err), http.StatusInternalServerError)
		return
	}

	var update struct {
		XMLName   xml.Name `xml:"user"`
		FirstName *string  `xml:"first-name"`
		LastName  *string  `xml:"last-name"`
		Email     *string  `xml:"email"`
	}

	err = xml.Unmarshal(b, &update)
	if err != nil {
		http.Error(w, fmt.Sprintf("users: %s", err), http.StatusInternalServerError)
		return
	}

	if update.FirstName != nil {
		u.FirstName = *update.FirstName
	}
	if update.LastName != nil {
		u.LastName = *update.LastName
	}
	if update.Email != nil {
		u.Email = *update.Email
	}

	err = user.Update(u)
	if err != nil {
		http.Error(w, fmt.Sprintf("users: %s", err), http.StatusInternalServerError)
		return
	}
}

func StartServerXML() error {
	// http.HandleFunc("/users/", allUserHandler)
	http.Handle("/users/", AccessLogWrap(UserXMLHandler))
	return http.ListenAndServe(":8000", nil)
}

var UserXMLHandler = http.StripPrefix("/users/", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path == "" {
		switch r.Method {
		case http.MethodGet:
			allUserXMLHandler(w, r)
		case http.MethodPost:
			createUserXMLHanlder(w, r)
		default:
			http.NotFound(w, r)
		}
	} else {
		id, err := strconv.Atoi(r.URL.Path)
		if err != nil {
			http.NotFound(w, r)
			return
		}

		switch r.Method {
		case http.MethodGet:
			getUserXMLHandler(id, w, r)
		case http.MethodPost, http.MethodPut:
			updateUserXMLHandler(id, w, r)
		case http.MethodDelete:
			deleteUserXMLHandler(id, w, r)
		}
	}
}))
