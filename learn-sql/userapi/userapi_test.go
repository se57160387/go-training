package userapi

import (
	"errors"
	"io/ioutil"
	"learn-sql/user"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gorilla/mux"

	"github.com/stretchr/testify/assert"
)

type mockUserService struct {
	UserService
	err error
}

func (m *mockUserService) FindByID(id int) (*user.User, error) {
	if m.err != nil {
		return nil, m.err
	}
	return &user.User{
		ID:        1,
		FirstName: "Weerasak",
		LastName:  "Chongnguluam",
		Email:     "singpor@gmail.com",
	}, nil
}

func (m *mockUserService) All() ([]user.User, error) {
	return []user.User{
		{
			ID:        1,
			FirstName: "Weerasak",
			LastName:  "Chongnguluam",
			Email:     "singpor@gmail.com",
		},
		{
			ID:        2,
			FirstName: "Kanokorn",
			LastName:  "Chongnguluam",
			Email:     "kanokorn@gmail.com",
		},
	}, nil
}

func TestGetUserHandler(t *testing.T) {
	h := &Handler{
		userService: &mockUserService{},
	}
	m := mux.NewRouter()
	m.HandleFunc("/users/{id}", h.getUser)

	r := httptest.NewRequest("GET", "http://lvm.me:8000/users/1", nil)
	w := httptest.NewRecorder()

	m.ServeHTTP(w, r)

	resp := w.Result()
	body, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusText(http.StatusOK), http.StatusText(resp.StatusCode))
	assert.Equal(t, "application/json", resp.Header.Get("Content-Type"))
	expectJSON := `{"id":1,"first_name":"Weerasak","last_name":"Chongnguluam","email":"singpor@gmail.com"}`
	assert.Equal(t, expectJSON, string(body))
}

func TestGetUserHandlerError(t *testing.T) {
	h := &Handler{
		userService: &mockUserService{
			err: errors.New("Error"),
		},
	}
	m := mux.NewRouter()
	m.HandleFunc("/users/{id}", h.getUser)

	r := httptest.NewRequest("GET", "http://lvm.me:8000/users/1", nil)
	w := httptest.NewRecorder()

	m.ServeHTTP(w, r)

	resp := w.Result()
	assert.Equal(t, http.StatusText(http.StatusInternalServerError), http.StatusText(resp.StatusCode))
}

func TestAllUserHandler(t *testing.T) {
	h := &Handler{
		userService: &mockUserService{},
	}
	m := mux.NewRouter()
	m.HandleFunc("/users/", h.allUser)

	r := httptest.NewRequest("GET", "http://lvm.me:8000/users/", nil)
	w := httptest.NewRecorder()

	m.ServeHTTP(w, r)
	resp := w.Result()
	body, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusText(http.StatusOK), http.StatusText(resp.StatusCode))
	assert.Equal(t, "application/json", resp.Header.Get("Content-Type"))
	expectJSON := `[{"id":1,"first_name":"Weerasak","last_name":"Chongnguluam","email":"singpor@gmail.com"},{"id":2,"first_name":"Kanokorn","last_name":"Chongnguluam","email":"kanokorn@gmail.com"}]`
	assert.Equal(t, expectJSON, string(body))
}
