package userapi

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"learn-sql/user"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

type UserService interface {
	FindByID(id int) (*user.User, error)
	All() ([]user.User, error)
	Insert(u *user.User) error
	Update(u *user.User) error
	Delete(u *user.User) error
}

type Handler struct {
	userService UserService
}

func writeError(w http.ResponseWriter, err error) bool {
	if err != nil {
		http.Error(w, "users: "+err.Error(), http.StatusInternalServerError)
		return true
	}
	return false
}
func writeJSON(w http.ResponseWriter, value interface{}) bool {
	b, err := json.Marshal(value)
	if writeError(w, err) {
		return true
	}
	w.Header().Set("Content-Type", "application/json")
	fmt.Fprintf(w, "%s", b)
	return false
}

func (h *Handler) getUser(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if writeError(w, err) {
		return
	}
	user, err := h.userService.FindByID(id)
	if writeError(w, err) {
		return
	}
	writeJSON(w, user)
}

func (h *Handler) allUser(w http.ResponseWriter, r *http.Request) {
	users, err := h.userService.All()
	if writeError(w, err) {
		return
	}
	if writeJSON(w, users) {
		return
	}
}
func (h *Handler) updateUser(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if writeError(w, err) {
		return
	}
	u, err := h.userService.FindByID(id)
	if writeError(w, err) {
		return
	}

	b, err := ioutil.ReadAll(r.Body)
	if writeError(w, err) {
		return
	}

	var update struct {
		FirstName *string `json:"first_name"`
		LastName  *string `json:"last_name"`
		Email     *string `json:"email"`
	}

	err = json.Unmarshal(b, &update)
	if writeError(w, err) {
		return
	}

	if update.FirstName != nil {
		u.FirstName = *update.FirstName
	}
	if update.LastName != nil {
		u.LastName = *update.LastName
	}
	if update.Email != nil {
		u.Email = *update.Email
	}

	if writeError(w, h.userService.Update(u)) {
		return
	}
}
func (h *Handler) deleteUser(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if writeError(w, err) {
		return
	}
	writeError(w, h.userService.Delete(&user.User{
		ID: id,
	}))

}
func (h *Handler) createUser(w http.ResponseWriter, r *http.Request) {
	b, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, fmt.Sprintf("users: %s", err), http.StatusInternalServerError)
		return
	}

	var u user.User
	err = json.Unmarshal(b, &u)
	if writeError(w, err) {
		return
	}

	err = h.userService.Insert(&u)
	if writeError(w, err) {
		return
	}
	w.WriteHeader(http.StatusCreated)
	if writeJSON(w, u) {
		return
	}
}
func StartServer(addr string, db *sql.DB) error {
	r := mux.NewRouter()
	h := &Handler{
		userService: &user.Service{
			DB: db,
		},
	}
	r.HandleFunc("/users/{id}", h.getUser).Methods("GET")
	r.HandleFunc("/users/{id}", h.updateUser).Methods("POST", "PUT")
	r.HandleFunc("/users/{id}", h.deleteUser).Methods("DELETE")
	r.HandleFunc("/users/", h.allUser).Methods("GET")
	r.HandleFunc("/users/", h.createUser).Methods("POST")

	return http.ListenAndServe(addr, r)
}
