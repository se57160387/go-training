package user

import (
	"database/sql"
	"encoding/xml"
	"errors"
	"log"

	_ "github.com/lib/pq"
)

var (
	db *sql.DB
)

func ConnectDB() {
	var err error
	const connStr = "user=postgres dbname=kbtg sslmode=disable"
	db, err = sql.Open("postgres", connStr)
	if err != nil {
		log.Fatal(err)
	}
}

type User struct {
	XMLName xml.Name `json:"-" xml:"user"`

	ID        int    `json:"id" xml:"id,attr"`
	FirstName string `json:"first_name" xml:"first-name"`
	LastName  string `json:"last_name" xml:"last-name"`
	Email     string `json:"email" xml:"email"`
}

func Insert(user *User) error {
	r := db.QueryRow("INSERT INTO users(first_name, last_name, email) VALUES ($1,$2,$3) RETURNING id", user.FirstName, user.LastName, user.Email)
	err := r.Scan(&user.ID)
	if err != nil {
		return err
	}
	return nil
}

func Update(user *User) error {
	_, err := db.Exec("UPDATE users SET first_name = $1, last_name = $2, email = $3 WHERE id = $4", user.FirstName, user.LastName, user.Email, user.ID)
	if err != nil {
		return err
	}
	return nil
}

func Delete(user *User) error {
	r, err := db.Exec("DELETE FROM users WHERE id = $1", user.ID)
	effect, err := r.RowsAffected()
	if err != nil {
		return err
	}
	if effect != 1 {
		return errors.New("user: delete not have effected row")
	}
	return nil
}

func FindByID(id int) (*User, error) {
	row := db.QueryRow("SELECT id, first_name, last_name, email FROM users WHERE id = $1", id)
	var u User
	err := row.Scan(&u.ID, &u.FirstName, &u.LastName, &u.Email)
	if err != nil {
		return nil, err
	}
	return &u, nil
}

func All() ([]User, error) {
	var users []User
	rows, err := db.Query("SELECT id, first_name, last_name, email FROM users")
	if err != nil {
		return nil, err
	}
	for rows.Next() {
		var u User
		err := rows.Scan(&u.ID, &u.FirstName, &u.LastName, &u.Email)
		if err != nil {
			return nil, err
		}
		users = append(users, u)
	}
	return users, nil
}

func Last() User {
	var u User
	row := db.QueryRow("SELECT id, first_name, last_name, email FROM users ORDER BY id DESC LIMIT 1")
	err := row.Scan(&u.ID, &u.FirstName, &u.LastName, &u.Email)
	if err != nil {
		log.Fatal(err)
	}
	return u
}

func ResetStorage() {
	_, err := db.Exec("TRUNCATE TABLE users RESTART IDENTITY;")
	if err != nil {
		log.Fatal(err)
	}
}
