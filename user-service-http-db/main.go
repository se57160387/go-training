package main

import (
	"log"
	"user-service/user"
	"user-service/userapi"
)

func main() {
	user.ConnectDB()
	log.Fatal(userapi.StartServer())
}
