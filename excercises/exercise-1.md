# เขียนโปรแกรมสรุปข้อมูลที่อ่านได้จาก record ที่อยู่ใน CSV

ให้อ่าน CSV ที่มีข้อมูลโครงสร้างแบบนี้

```csv
uid, card_holder_name, amount_subunits
chrg_e9b1b58e3f114f5f8c9c2768df1116ec, John Doe, 100000
chrg_83c41e7ae7614aff8820242066c9a471, Jan Jackson, 150000
chrg_1b9c7ef19a274a1f8edfcda2562518c8, John Doe, 2500050
chrg_83c41e7ae7614aff8820242066c9a471, Jan Jackson, 1700000
chrg_0e96f6fd5abd4aabb2829eceee2c75f5, Weerasak Chongnguluam, 234560
```

โดยที่ค่า amount\_subunits นั้นเป็นเงินในหน่วยสตางค์

หลังจากนั้นให้แสดงผลข้อมูลสรุปว่าแต่ละ card_holder_name มียอดจ่ายเงินรวมเท่าไหร่ โดยเรียงลำดับจากคนจ่ายเงินมากที่สุดไปหาน้อยที่สุดแบบนี้

```
Report Credit Card usages

John Doe => 26000.50 ฿
Jan Jackson => 18500.00 ฿
Weerasak Chongnguluam => 2345.60 ฿
```

## Extra 1: จัดเรียง report ให้เป็นระเบียบแบบนี้

```
Report Credit Card usages

John Doe              => 26000.50 ฿
Jan Jackson           => 18500.00 ฿
Weerasak Chongnguluam =>  2345.60 ฿
```

## Extra 2: เขียนไฟล์ผลลัพธ์ report.csv เพื่อเก็บผลที่ได้ในไฟล์ csv แบบนี้

```csv
card_holder_name, amount
John Doe, 26000.50
Jan Jackson, 18500.00
Weerasak Chongnguluam, 2345.60
```
