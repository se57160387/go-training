package user

import (
	"database/sql"
	"log"
	"testing"

	"github.com/stretchr/testify/assert"
)

func connectDB() *sql.DB {
	const connStr = "user=postgres dbname=kbtg sslmode=disable"
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		log.Fatal(err)
	}
	return db
}
func TestInsert(t *testing.T) {
	m := Manager{
		DB: connectDB(),
	}
	defer m.ResetStorage()

	u := User{
		FirstName: "Weerasak",
		LastName:  "Chongnguluam",
		Email:     "singpor@gmail.com",
	}
	err := m.Insert(&u)
	assert.NoError(t, err)
	assert.Equal(t, u, m.Last())

	u = User{
		FirstName: "Kanokorn",
		LastName:  "Chongnguluam",
		Email:     "kanokorn@gmail.com",
	}
	err = m.Insert(&u)
	assert.NoError(t, err)
	assert.Equal(t, u, m.Last())
}

func TestUpdate(t *testing.T) {
	m := Manager{
		DB: connectDB(),
	}
	defer m.ResetStorage()

	u := User{
		FirstName: "Weerasak",
		LastName:  "Chongnguluam",
		Email:     "singpor@gmail.com",
	}
	err := m.Insert(&u)
	assert.NoError(t, err)

	u = m.Last()
	u.Email = "singpor@outlook.com"
	err = m.Update(&u)
	assert.NoError(t, err)
	assert.Equal(t, u, m.Last())
}

func TestFindByID(t *testing.T) {
	m := Manager{
		DB: connectDB(),
	}
	defer m.ResetStorage()

	u := User{
		FirstName: "Weerasak",
		LastName:  "Chongnguluam",
		Email:     "singpor@gmail.com",
	}
	err := m.Insert(&u)
	assert.NoError(t, err)

	r, err := m.FindByID(1)
	assert.NoError(t, err)
	assert.Equal(t, u, *r)

	r, err = m.FindByID(2)
	assert.Nil(t, r)
	assert.Error(t, err)
}

func TestDelete(t *testing.T) {
	m := Manager{
		DB: connectDB(),
	}
	defer m.ResetStorage()

	u := User{
		FirstName: "Weerasak",
		LastName:  "Chongnguluam",
		Email:     "singpor@gmail.com",
	}
	err := m.Insert(&u)
	assert.NoError(t, err)

	r, err := m.FindByID(1)
	assert.NoError(t, err)

	err = m.Delete(r)
	assert.NoError(t, err)

	err = m.Delete(r)
	assert.Error(t, err)
}

func TestAll(t *testing.T) {
	m := Manager{
		DB: connectDB(),
	}
	defer m.ResetStorage()

	m.Insert(&User{
		FirstName: "Weerasak",
		LastName:  "Chongnguluam",
		Email:     "singpor@gmail.com",
	})
	m.Insert(&User{
		FirstName: "Kanokorn",
		LastName:  "Chongnguluam",
		Email:     "kanokorn@gmail.com",
	})

	users, err := m.All()
	assert.NoError(t, err)
	assert.Equal(t, []User{
		{
			ID:        1,
			FirstName: "Weerasak",
			LastName:  "Chongnguluam",
			Email:     "singpor@gmail.com",
		},
		{
			ID:        2,
			FirstName: "Kanokorn",
			LastName:  "Chongnguluam",
			Email:     "kanokorn@gmail.com",
		},
	}, users)
}
