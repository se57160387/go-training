package main

import (
	"database/sql"
	"log"
	"user-service/user"
	"user-service/userapi"
)

func main() {
	const connStr = "user=postgres dbname=kbtg sslmode=disable"
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		log.Fatal(err)
	}

	log.Fatal(userapi.StartServer(&userapi.Handler{
		M: &user.Manager{
			DB: db,
		},
	}))
}
