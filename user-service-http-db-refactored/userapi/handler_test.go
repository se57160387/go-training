package userapi

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"user-service/user"

	"github.com/stretchr/testify/assert"
)

type fakeManager struct {
	Manager
	err         error
	users       []user.User
	idSeq       int
	createdUser *user.User
}

func (m *fakeManager) All() ([]user.User, error) {
	if m.err != nil {
		return nil, m.err
	}
	return m.users, nil
}

func (m *fakeManager) FindByID(id int) (*user.User, error) {
	if m.err != nil {
		return nil, m.err
	}
	u := m.users[id-1]
	return &u, nil
}

func (m *fakeManager) Insert(u *user.User) error {
	if m.err != nil {
		return m.err
	}
	m.createdUser = u
	m.idSeq++
	m.createdUser.ID = m.idSeq
	return nil
}

func TestAllUserHandler(t *testing.T) {
	h := &Handler{
		M: &fakeManager{
			users: []user.User{
				{
					ID:        1,
					FirstName: "Weerasak",
					LastName:  "Chongnguluam",
					Email:     "singpor@gmail.com",
				},
				{
					ID:        2,
					FirstName: "Kanokorn",
					LastName:  "Chongnguluam",
					Email:     "kanokorn@gmail.com",
				},
			},
		},
	}

	r := httptest.NewRequest("GET", "http://lvm.me:8000/users/", nil)
	w := httptest.NewRecorder()
	h.allUserHandler(w, r)
	resp := w.Result()
	body, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusText(http.StatusOK), http.StatusText(resp.StatusCode))
	assert.Equal(t, "application/json", resp.Header.Get("Content-Type"))
	expectJSON := `[{"id":1,"first_name":"Weerasak","last_name":"Chongnguluam","email":"singpor@gmail.com"},{"id":2,"first_name":"Kanokorn","last_name":"Chongnguluam","email":"kanokorn@gmail.com"}]`
	assert.Equal(t, expectJSON, string(body))
}

func TestGetUserHandler(t *testing.T) {
	h := &Handler{
		M: &fakeManager{
			users: []user.User{
				{
					ID:        1,
					FirstName: "Weerasak",
					LastName:  "Chongnguluam",
					Email:     "singpor@gmail.com",
				},
				{
					ID:        2,
					FirstName: "Kanokorn",
					LastName:  "Chongnguluam",
					Email:     "kanokorn@gmail.com",
				},
			},
		},
	}

	r := httptest.NewRequest("GET", "http://lvm.me:8000/users/1", nil)
	w := httptest.NewRecorder()
	h.getUserHandler(1, w, r)
	resp := w.Result()
	body, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusText(http.StatusOK), http.StatusText(resp.StatusCode))
	assert.Equal(t, "application/json", resp.Header.Get("Content-Type"))
	expectJSON := `{"id":1,"first_name":"Weerasak","last_name":"Chongnguluam","email":"singpor@gmail.com"}`
	assert.Equal(t, expectJSON, string(body))
}

func TestDeleteUserHandler(t *testing.T) {
	// Excercise
}

func TestCreateUserHandler(t *testing.T) {
	h := &Handler{
		M: &fakeManager{},
	}
	jsonBody := `{"first_name":"Weerasak","last_name":"Chongnguluam","email":"singpor@gmail.com"}`
	r := httptest.NewRequest("POST", "http://lvm.me:8000/users/", strings.NewReader(jsonBody))
	w := httptest.NewRecorder()
	h.createUserHanlder(w, r)
	resp := w.Result()
	body, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusText(http.StatusCreated), http.StatusText(resp.StatusCode))
	assert.Equal(t, "application/json", resp.Header.Get("Content-Type"))
	expectJSON := `{"id":1,"first_name":"Weerasak","last_name":"Chongnguluam","email":"singpor@gmail.com"}`
	assert.Equal(t, expectJSON, string(body))
}

func TestUpdateUserHandler(t *testing.T) {
	// Excercise
}
