package account

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func reset() {
	idSeq = 0
	storage = make(map[int]Account)
}

func TestNewAccount(t *testing.T) {
	acc := New("Weerasak Chongnguluam")
	expect := &Account{
		ID:      0,
		Name:    "Weerasak Chongnguluam",
		balance: 0,
	}
	assert.Equal(t, expect, acc)
}

func TestSaveAccount(t *testing.T) {
	defer reset()

	acc := New("Weerasak Chongnguluam")
	Save(acc)
	expect := &Account{
		ID:      1,
		Name:    "Weerasak Chongnguluam",
		balance: 0,
	}
	assert.Equal(t, expect, acc)
}

func TestFindByName(t *testing.T) {
	defer reset()

	Save(New("Weerasak Chongnguluam"))
	Save(New("Kanokorn Chongnguluam"))
	acc := FindByName("Weerasak Chongnguluam")
	assert.Equal(t, &Account{ID: 1, Name: "Weerasak Chongnguluam", balance: 0}, acc)
	acc = FindByName("Kanokorn Chongnguluam")
	assert.Equal(t, &Account{ID: 2, Name: "Kanokorn Chongnguluam", balance: 0}, acc)
	acc = FindByName("NO")
	assert.Nil(t, acc)
}

func TestWithdraw(t *testing.T) {
	acc := &Account{
		balance: 100000,
	}
	acc.Withdraw(50000)
	assert.Equal(t, 50000, acc.Balance())
}

func TestDeposit(t *testing.T) {
	acc := &Account{
		balance: 100000,
	}
	acc.Deposit(50000)
	assert.Equal(t, 150000, acc.Balance())
}
