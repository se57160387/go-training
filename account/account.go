package account

// ### Exercises: account package
// สร้าง package ที่จัดการ account ของบัญชีออมทรัพย์
// โดย package account ต้องมี function
// - `func New(name string) *Account` // สำหรับสร้าง account ใหม่
// - `func Save(account *Account)` // สำหรับ save/update account เอาไว้
// - `func FindByName(name string) *Account` // ค้นหา account ที่เก็บเอาไว้

// ส่วน *Account นั้นให้มี Method สำหรับ ฝาก และ ถอน เงิน และเรียกดู Balance ดังนี้
// ```go
// func (a *Account) Withdraw(amount int)
// func (a *Account) Deposit(amount int)
// func (a *Account) Balance() int
// ```

var (
	idSeq   int
	storage = make(map[int]Account)
)

type Account struct {
	ID      int
	Name    string
	balance int
}

func New(name string) *Account {
	return &Account{
		Name: name,
	}
}

func Save(acc *Account) {
	idSeq++
	acc.ID = idSeq
	storage[idSeq] = *acc

}

func FindByName(name string) *Account {
	for _, v := range storage {
		if name == v.Name {
			return &v
		}
	}
	return nil
}

func (acc *Account) Withdraw(amount int) {
	acc.balance -= amount
}

func (acc *Account) Deposit(amount int) {
	acc.balance += amount
}

func (acc *Account) Balance() int {
	return acc.balance
}
