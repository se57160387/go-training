package main

import (
	"blog-webapp/pq/post"
	"log"
)

func main() {
	post.ConnectDB()
	log.Fatal(startServer())
}
