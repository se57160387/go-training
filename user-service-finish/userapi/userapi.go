package userapi

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"user-service/user"
)

func allUserHandler(w http.ResponseWriter, r *http.Request) {
	users, err := user.All()
	if err != nil {
		http.Error(w, fmt.Sprintf("users: %s", err), http.StatusInternalServerError)
		return
	}

	byteUsers, err := json.Marshal(users)
	if err != nil {
		http.Error(w, fmt.Sprintf("users: %s", err), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	fmt.Fprintf(w, "%s", byteUsers)
}

func createUserHanlder(w http.ResponseWriter, r *http.Request) {
	b, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, fmt.Sprintf("users: %s", err), http.StatusInternalServerError)
		return
	}

	var u user.User
	err = json.Unmarshal(b, &u)
	if err != nil {
		http.Error(w, fmt.Sprintf("users: %s", err), http.StatusInternalServerError)
		return
	}

	err = user.Insert(&u)
	if err != nil {
		http.Error(w, fmt.Sprintf("users: %s", err), http.StatusInternalServerError)
		return
	}

	byteUser, err := json.Marshal(u)
	if err != nil {
		http.Error(w, fmt.Sprintf("users: %s", err), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	fmt.Fprintf(w, "%s", byteUser)
}

func getUserHandler(id int, w http.ResponseWriter, r *http.Request) {
	u, err := user.FindByID(id)
	if err != nil {
		http.Error(w, fmt.Sprintf("users: %s", err), http.StatusNotFound)
		return
	}

	byteUser, err := json.Marshal(u)
	if err != nil {
		http.Error(w, fmt.Sprintf("users: %s", err), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	fmt.Fprintf(w, "%s", byteUser)
}

func deleteUserHandler(id int, w http.ResponseWriter, r *http.Request) {
	err := user.Delete(&user.User{
		ID: id,
	})
	if err != nil {
		http.Error(w, fmt.Sprintf("users: %s", err), http.StatusInternalServerError)
		return
	}
}

func updateUserHandler(id int, w http.ResponseWriter, r *http.Request) {
	u, err := user.FindByID(id)
	if err != nil {
		http.Error(w, fmt.Sprintf("users: %s", err), http.StatusNotFound)
		return
	}

	b, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, fmt.Sprintf("users: %s", err), http.StatusInternalServerError)
		return
	}

	var update struct {
		FirstName *string `json:"first_name"`
		LastName  *string `json:"last_name"`
		Email     *string `json:"email"`
	}

	err = json.Unmarshal(b, &update)
	if err != nil {
		http.Error(w, fmt.Sprintf("users: %s", err), http.StatusInternalServerError)
		return
	}

	if update.FirstName != nil {
		u.FirstName = *update.FirstName
	}
	if update.LastName != nil {
		u.LastName = *update.LastName
	}
	if update.Email != nil {
		u.Email = *update.Email
	}

	err = user.Update(u)
	if err != nil {
		http.Error(w, fmt.Sprintf("users: %s", err), http.StatusInternalServerError)
		return
	}
}

func StartServer() error {
	// http.HandleFunc("/users/", allUserHandler)
	http.Handle("/users/", http.StripPrefix("/users/", http.HandlerFunc(userHandler)))
	return http.ListenAndServe(":8000", nil)
}

func userHandler(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path == "" {
		switch r.Method {
		case http.MethodGet:
			allUserHandler(w, r)
		case http.MethodPost:
			createUserHanlder(w, r)
		default:
			http.NotFound(w, r)
		}
	} else {
		id, err := strconv.Atoi(r.URL.Path)
		if err != nil {
			http.NotFound(w, r)
			return
		}

		switch r.Method {
		case http.MethodGet:
			getUserHandler(id, w, r)
		case http.MethodPost, http.MethodPut:
			updateUserHandler(id, w, r)
		case http.MethodDelete:
			deleteUserHandler(id, w, r)
		}
	}
}
