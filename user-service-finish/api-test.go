// +build api-test

package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
)

func main() {
	jsonBody := `{"email": "jan@hotmail.com"}`
	resp, _ := http.Post("http://localhost:8000/users/4",
		"application/json",
		strings.NewReader(jsonBody))

	fmt.Println(http.StatusText(resp.StatusCode))

	resp, _ = http.Get("http://localhost:8000/users/4")
	b, _ := ioutil.ReadAll(resp.Body)
	fmt.Printf("%s\n", b)
}
