package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net/http"
)

type helloReq struct {
	Name string `json:"name"`
}

type helloResp struct {
	Msg string `json:"msg"`
}

func main() {
	var portOpt string
	flag.StringVar(&portOpt, "port", ":8000", "service port")
	flag.Parse()

	http.HandleFunc("/hello", func(w http.ResponseWriter, r *http.Request) {
		var err error
		var req helloReq
		err = json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			log.Printf("json-decode: %s", err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		msg := fmt.Sprintf("Hello, %s", req.Name)
		err = json.NewEncoder(w).Encode(&helloResp{Msg: msg})
		if err != nil {
			log.Printf("json-encode: %s", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	})

	err := http.ListenAndServe(portOpt, nil)
	if err != nil {
		log.Fatalf("server: %s", err)
	}
}
