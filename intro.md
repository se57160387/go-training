# Introduction

Go เริ่มพัฒนาโดยทีมงานของ Google เมื่อปี 2007 เมื่อแก้ปัญหาที่เจอในการพัฒนาซอฟแวร์ infrastructure ของ Google

Go เป็นภาษาที่ compile เป็นภาษาเครื่อง รองรับการทำงานแบบ concurrent มีระบบ garbage-collector ช่วยจัดการหน่วยความจำ และ เป็น static type

## ปัญหาที่ Google เจอ
* slow builds
* uncontrolled dependencies
* each programmer using a different subset of the languange
* poor program understanding (code hard to read, poorly documented, and so on)
* duplication of effort
* cost of updates

เพื่อแก้ปัญหาที่ลิสต์มา Go จึงถูกออกแบบให้มีคุณสมบัติดังต่อไปนี้
* Clear dependencies
* Clear syntax
* Clear semantics
* Composition over inheritance
* Simplicity provided by the programming model (garbage collection, cuncurrency)
* Easy tooling (the go tool, gofmt, godoc, gofix, ...)

## เครื่องมือที่ใช้ในการพัฒนาโปรแกรมด้วยภาษา Go
* [Go](http://golang.org/dl/)
* [git](https://git-scm.com)
* [vscode](https://code.visualstudio.com)

## Environment Variable ที่สำคัญสำหรับ Go
* GOROOT ชี้ไปที่ path ของ Go ที่เก็บ compiler เครื่องมือต่าง ๆ และ standard package
* GOPATH ชี้ไปที่ path ที่เป็น working space ของ Go สำหรับเก็บ package ที่ติดตั้งผ่าน go get และ โค้ดของ project ที่เราใช้งาน
* GOBIN ชี้ไปที่ path สำหรับเก็บโปรแกรมที่ติดตั้งผ่าน go get หรือ go install
* PATH เราต้องการหนดให้ GOBIN อยู่ภายใน PATH ด้วย หรือ ถ้าใครติดตั้ง Go ด้วยการโหลด tar หรือ zip ก็ต้องกำหมด GOROOT/bin ใน PATH ด้วยเช่นกัน

การตั้งค่า Env แต่ละ OS ไม่เหมือนกัน

## Hello, World

ทดลองสร้างโปรแกรมง่าย ๆ ที่แสดง Hello, World โดยเริ่มสร้าง directory hello หลังจากนั้นใช้ go mod เพื่อกดหมด module ใหม่ แล้วเพิ่มโค้ดในไฟล์ `main.go`

```shell
mkdir hello

cd hello

go mod init hello
```

```go
// main.go
package main

import (
        "fmt"
)

func main() {
        fmt.Println("Hello, World")
}
```

ทดลองรันโปแกรมด้วย go run ซึ่งจะทำการ compile แล้ว run โปรแกรมทันที โดยไม่สร้าง binary ให้เราใหม่

```shell
go run main.go
```

เราสามารถใช้ go build เพื่อทำการ compile โปรแกรมเป็น binary file ที่เราต้องการแล้วรันได้จาก binary file นั้น

```shell
go build

./hello
```

เราสามารถใช้ go install เพื่อทำการ compile ได้ binary แล้ว binary นั้นจะถูกติดตั้งไว้ที่ GOBIN

```shell
go install

hello
```

### องค์ประกอบของโปรแกรม Hello, World
โปรแกรมที่ถูกเขียนด้วย Go จะถูกแบ่งออกเป็น package ซึ่งแต่ละ package จะประกอบไปด้วยหลาย ๆ ไฟล์ .go และในแต่ละไฟล์ ข้องประกาศว่าเป็นโค้ดที่อยู่ภายใต้ package ชื่ออะไร เช่นไฟล์ที่เราเพิ่มเขียนไปนั้นอยู่ภายใต้ package ที่ชื่อว่า `main`

โปรแกรม Go จะเริ่มทำงานที่ func ที่ชื่อว่า `func main()` และ ภายใต้ `package main` เท่านั้น ซึ่งถ้าโค้ดภายใน package เราไม่ได้ชื่อนี้ เวลาสั่ง go build หรือ go install จะไม่ได้ถูก compile เป็น executable binary จะได้แต่ object code เท่านั้น

โค้ดใน func main เราต้องการแสดงคำว่า Hello, World ออกที่หน้าจอ แต่ว่า Go เตรียมคำสั่งเกี่ยวกับการแสดงผลเหล่านี้เอาไว้ที่ package อื่นที่ชื่อว่า `fmt` การจะเรียกใช้งาน package อื่นเราต้อง import package นั้นเข้ามาก่อน โดยใช้ คำสั่ง import

เวลาเรียกใช้ func อื่นที่อยู่คนละ package เราต้องใช้ชื่อ package จุด แล้วตามด้วยชื่อ func นั้นเช่น `fmt.Println` เราเรียกใช้ Println ที่ถูกสร้างไว้ใน fmt

## Hello World 2.0

```go
package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net/http"
)

type helloReq struct {
	Name string `json:"name"`
}

type helloResp struct {
	Msg string `json:"msg"`
}

func main() {
	var portOpt string
	flag.StringVar(&portOpt, "port", ":8000", "service port")
	flag.Parse()

	http.HandleFunc("/hello", func(w http.ResponseWriter, r *http.Request) {
		var err error
		var req helloReq
		err = json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			log.Printf("json-decode: %s", err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		msg := fmt.Sprintf("Hello, %s", req.Name)
		err = json.NewEncoder(w).Encode(&helloResp{Msg: msg})
		if err != nil {
			log.Printf("json-encode: %s", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	})

	err := http.ListenAndServe(portOpt, nil)
	if err != nil {
		log.Fatalf("server: %s", err)
	}
}
```

## ตัวแปร และ ประเภทข้อมูล

### ตัวแปร
ตัวแปร ช่วยให้เราเก็บข้อมูลที่ต่าง ๆ ระหว่างเราประมวลผลโปรแกรม และ เราอ้างอิงข้อมูลนั้นผ่านทางชื่อตัวแปร

Go เป็นภาษา static type นั่นคือ ตัวแปรของ Go จะต้องระบุว่าเป็นตัวแปรสำหรับ type ไหน เพื่อจำกัดว่าตัวแปรนั้นเก็บข้อมูลได้แค่ type นั้นเท่านั้น แล้ว compiler จะช่วยเช็คให้เรา ถ้าเราเอาตัวแปรไปใช้งานผิดจากที่ type นั้นทำได้

### ประเภทข้อมูล
Go มีประเภทข้อมูลพื้นฐานให้เราโดยแบ่งเป็นกลุ่มของ
* int จำนวนเต็ม 1, 2, 3
* float64 ทศนิยม 1.5, 2.6, 3.7
* complex64 จำนวนเชิงซ้อน (ได้ใช้้น้อยในงานปกติ ใช้เยอะในงานคำนวณทางวิทยาศาสตร์) 5 + 6i
* string เก็บตัวหนังสือ "สวัสดีครับ"
* bool เก็บค่า true / false

### การประกาศตัวแปร
syntax ในการประกาศตัวแปรเบื้องต้นของ Go คือ

```go
var variableName variableType
```

เช่น

```go
package main

import (
	"fmt"
)

func main() {
	var n int     // zero value is 0
	var f float64 // zero value is 0.0
	var s string  // zero value is ""
	var b bool    // zero value is false

	fmt.Println("n:", n)
	fmt.Println("f:", f)
	fmt.Println("s:", s)
	fmt.Println("b:", b)
}
```

### รวมการประกาศตัวแปรเข้าด้วยกันด้วย var เดียว
```go
package main

import (
	"fmt"
)

func main() {
	var (
		n int     // zero value is 0
		f float64 // zero value is 0.0
		s string  // zero value is ""
		b bool    // zero value is false
	)

	fmt.Println("n:", n)
	fmt.Println("f:", f)
	fmt.Println("s:", s)
	fmt.Println("b:", b)
}
```

### ประกาศตัวแปรพร้อมกำหนดค่าเริ่มต้น
```go
package main

import (
	"fmt"
)

func main() {
	var name string = "John"

	fmt.Println("Hello", name)
}
```

เราสามารถตัดชื่อ type ออกได้ เพราะ เมื่อเรากำหนดค่าเริ่มต้น Go compiler รู้เองว่าตัวแปรควรเป็น type ไหน
```go
package main

import (
	"fmt"
)

func main() {
	var name = "John"

	fmt.Println("Hello", name)
}
```
เราสามารถใช้การประกาศตัวแปร และ กำหนดค่าเริ่มต้นแบบสั้น ๆ ได้โดยใช้ `:=` แต่มีข้อจำกัดคือใช้กับตัวแปรที่ประกาศใน func เท่านั้น
```go
package main

import (
	"fmt"
)

func main() {
	name := "John"

	fmt.Println("Hello", name)
}
```

## Operator สำหรับตัวเลข เบื้องต้น

ตัวเลข ไม่ว่าจะเป็น int, float64 สามารถใช้ operator ในการคำนวณพื้นฐานต่อไปนี้ได้

* `+`
* `-`
* `*`
* `/`
* `%` (ใช้ได้แค่ จำนวนเต็ม)

ตัวอย่าง

```go
package main

import (
	"fmt"
)

func main() {
	const serviceChargeRate = 10.0
	const vatRate = 7.0

	price := 1150.0
	serviceCharge := price * serviceChargeRate / 100.0
	vat := price * vatRate / 100.0
	total := price + serviceCharge + vat

	fmt.Printf("Price : %.2f\n", price)
	fmt.Printf("Service charge %.2f%% : %.2f\n", serviceChargeRate, serviceCharge)
	fmt.Printf("Vat %.2f%% : %.2f\n", vatRate, vat)
	fmt.Printf("Total: %.2f\n", total)
}
```

## Operator สำหรับ string เบื้องต้น

เราสามารถใช้ `+` เพื่อต่อ string สองตัวเข้าด้วยกันได้ ส่วนการทำงานที่ซับซ้อนขึ้นกับ string เราจะเรียกใช้ package อื่นช่วยเช่น `strings` และ `fmt`

```go
package main

import (
	"fmt"
)

func main() {
	name := "John"

	fmt.Println("Hello " + name)
}
```

## Operator สำหรับ bool เบื้องต้น

* `!` (not)
* `&&` (and)
* `||` (or)

```go
package main

import (
	"fmt"
)

func main() {
	fmt.Println("not true is", !true)
	fmt.Println("not false is", !true)

	fmt.Println("true or true is", true || true)
	fmt.Println("true or false is", true || false)
	fmt.Println("false or true is", false || true)
	fmt.Println("false or false is", false || false)

	fmt.Println("true and true is", true && true)
	fmt.Println("true and false is", true && false)
	fmt.Println("false and true is", false && true)
	fmt.Println("false and false is", false && false)

}
```

## Operator สำหรับการเปรียบเทียบ

* `>`
* `<`
* `>=`
* `<=`
* `==`
* `!=`

การทำงานของ operator เหล่านี้ได้ผลลัพธ์กลับมาเป็นค่า bool นั่นคือเป็น true หรือ false นั่นเอง

```go
package main

import (
	"fmt"
)

func main() {
	a := 10
	b := 20

	fmt.Println("a > b is", a > b)
	fmt.Println("a < b is", a < b)
	fmt.Println("a >= b is", a >= b)
	fmt.Println("a <= b is", a <= b)
	fmt.Println("a == b is", a == b)
	fmt.Println("a != b is", a != b)

	s := "Hello"
	t := "Hella"

	fmt.Println("s > t is", s > t)
	fmt.Println("s < t is", s < t)
	fmt.Println("s >= t is", s >= t)
	fmt.Println("s <= t is", s <= t)
	fmt.Println("s == t is", s == t)
	fmt.Println("s != t is", s != t)
}
```

## Branch และ Loop

Go มีคำสั่งที่ทำให้โปรแกรมเลือกการทำงานตามเงื่อนไข (Branch) และ การทำให้โปรแกรมทำซ้ำตามเงื่อนไข

### Branch

สำหรับการเลือกทำงานตามเงื่อนไขนั้นใช้โครงสร้างคำสั่ง if หรือ if else หรือ switch case ช่วย

```go
	if (เงื่อนไข) {
		ทำเมื่อเป็นจริง
	}
```

```go
	if (เงื่อนไข) {
		ทำเมื่อเป็นจริง
	} else {
		ทำเมื่อเป็นเท็จ
	}
```

```go
package main

import "fmt"

func main() {
	currentUser := "john"

	if currentUser != "root" {
		fmt.Println("Cannot Access")
	} else {
		fmt.Println("Logged as root")
	}
}
```

```go
package main

import "fmt"

func main() {
	currentUser := "john"

	if currentUser != "root" {
		fmt.Println("Cannot Access")
		return
	}

	fmt.Println("Logged as root")
}
```

```go
package main

import "fmt"

func main() {
	direction := "N"
	switch direction {
	case "N":
		fmt.Println("NORTH")
	case "S":
		fmt.Println("SOUTH")
	case "E":
		fmt.Println("EAST")
	case "W":
		fmt.Println("WEST")
	case "NE":
		fmt.Println("NORTH EAST")
	case "NW":
		fmt.Println("NORTH WEST")
	case "SE":
		fmt.Println("SOUTH EAST")
	case "SW":
		fmt.Println("SOUTH WEST")

	default:
		fmt.Println("WRONG DIRECTION")
	}
}
```

นอกจากรูปแบบการใช้งานที่ตามตัวอย่างที่ผ่านมาแล้วนั้น Go สามารถกำหนดตัวแปรใน block ของ if และ switch ก่อน เริ่มปีกกาเปิดได้ เช่น

```go
package main

import "fmt"

func main() {
	if currentUser := "john"; currentUser != "root" {
		fmt.Println("Cannot Access")
		return
	}

	fmt.Println("Logged as root")
}
```

```go
package main

import "fmt"

func main() {

	switch direction := "N"; direction {
	case "N":
		fmt.Println("NORTH")
	case "S":
		fmt.Println("SOUTH")
	case "E":
		fmt.Println("EAST")
	case "W":
		fmt.Println("WEST")
	case "NE":
		fmt.Println("NORTH EAST")
	case "NW":
		fmt.Println("NORTH WEST")
	case "SE":
		fmt.Println("SOUTH EAST")
	case "SW":
		fmt.Println("SOUTH WEST")

	default:
		fmt.Println("WRONG DIRECTION")
	}
}
```

### Loop

สำหรับการทำงานแบบวนซ้ำนั้น Go มี keyword เดียวคือ `for` แต่สามารถเขียนได้หลายแบบ เอาแบบหลัก ๆ ที่คุณชินกันก็แบบนี้

```go
package main

import "fmt"

func main() {
	for i := 1; i <= 10; i++ {
		fmt.Println(i)
	}
}
```

คือมีส่วนสร้างตัวแปรและกำหนดค่า จากนั้นเป็นเงื่อนไขของการวนซ้ำว่าจะยังคงทำไปจนถึงเมื่อไหร่ แล้วก็ส่วนที่เปลี่ยนค่าของตัวแปร

หรือจะเขียนได้อีกแบบคือละส่วนกำหนดค่าและเปลี่ยนค่า ให้เหลือแต่เงื่อนไขแบบนี้

```go
package main

import "fmt"

func main() {
	i := 1
	for i <= 10 {
		fmt.Println(i)
		i++
	}
}
```

### Exercise
* หาผลบวกเลขคี่ ตั้งแต่ 1 ถึง 100

## ประเภทข้อมูลแบบ Slice และ Map

หลังจากเรารู้ข้อมูลเบื้องต้น ได้แต่ int, float64, string, bool ไปแล้ว ต่อไปเราจะไปดูประเภทข้อมูลที่ทำให้เราสามารถเก็บลิสต์ของข้อมูลอื่นหลายๆค่า นั่นคือ slice

slice ก็คือตัวแปรที่เก็บข้อมูลเป็นลำดับ เช่น 1, 2, 3, 4

ตัวอย่างการสร้างตัวแปร slice
```go
var nums []int
```

เป็นการสร้างตัวแปร nums ที่ระบุว่าเก็บลิสต์ของตัวเลขประเภทจำนวนเต็ม แต่การสร้างแค่นี้เราจะ slice ที่มีค่าเป็น nil ซึ่งยังไม่มีข้อมูลอะไร

การเพิ่มข้อมูลเข้าไปใน slice จะใช้ฟังก์ชันที่ชื่อ `append`

```go
package main

import "fmt"

func main() {
	var nums []int

	nums = append(nums, 1)
	nums = append(nums, 2)

	fmt.Println(nums)
}
```

ข้อมูลที่เราเพิ่มไปที่ slice ด้วย append จะต่อท้ายไปเรื่อยๆ

สำหรับวิธีการเข้าถึงข้อมูลแต่ละค่าที่อยู่ใน slice นั้นเราจะใช้ index โดยที่ค่าแรกเริ่มที่ index 0 ตัวอย่างเช่น

```go
package main

import "fmt"

func main() {
	var nums []int

	nums = append(nums, 1)
	nums = append(nums, 2)

	fmt.Println(nums[0])
	fmt.Println(nums[1])
}
```

ถ้าเราเรียกใช้ index ที่ยังไม่มีใน slice จะทำให้เกิด panic แล้วก็โปรแกรมเราจะหยุดทำงาน

```go
package main

import "fmt"

func main() {
	var nums []int

	nums = append(nums, 1)
	nums = append(nums, 2)

	fmt.Println(nums[0])
	fmt.Println(nums[1])
	// fmt.Println(nums[2]) // uncomment will panic
}
```

เราสามารถหาว่า slice เรามีจำนวนสมาชิกภายใน slice อยู่เท่าไหร่โดยใช้ function `len`

```go
package main

import "fmt"

func main() {
	var nums []int // ค่าเป็น nil

	fmt.Println("Len:", len(nums))

	nums = append(nums, 1)
	nums = append(nums, 2)

	fmt.Println("Len:", len(nums))
}
```

การประกาศตัวแปร slice ที่เราทำไปก่อนหน้านี้ เริ่มต้นคือ slice จะไม่มี สมาชิกใด ๆ แต่ถ้าเราต้องการสร้างตัวแปร slice พร้อมกำหนดสมาชิกเริ่มต้นเอาไว้เลย เราสามารถทำได้แบบนี้

```go
package main

import "fmt"

func main() {
	nums := []int{1, 2}
	fmt.Println(nums)
}
```

สำหรับการวนซ้ำเข้าไปในแต่ละสมาชิกของ slice นั้นเราก็ยังคงใช้ for ช่วยได้เช่นกัน แบบแรกคือ

```go
package main

import "fmt"

func main() {
	names := []string{
		"Weerasak",
		"Kanokorn",
	}

	for i := 0; i < len(names); i++ {
		fmt.Println(i, ":", names[i])
	}
}
```

แต่จริง ๆ แล้ว Go มีวิธีการวนซ้ำใน slice ที่ไม่ต้องมาหา len เองแบบนี้คือใช้ range เข้าช่วย เช่น

```go
package main

import "fmt"

func main() {
	names := []string{
		"Weerasak",
		"Kanokorn",
	}

	for i, name := range names {
		fmt.Println(i, ":", name)
	}
}
```

ซึ่งเวลาใช้ range อยู่หน้า slice แล้วเราจะใช้ `:=` เพื่อกำหนดค่าให้กับตัวแปร 2 ตัวทางด้านซ้ายของ `:=` ซึ่งตัวแรกเป็น index ในการวนซ้ำแต่ละรอบ และ ตัวที่ 2 เป็นค่าของสมาชิกในตำแหน่ง index นั้นๆ

ถ้าเราไม่ต้องการใช้ค่าใดค่าหนึ่ง เช่นอยากใช้แค่ค่าสมาชิกในลูป เราสามารถใช้ _ แทนตัวแปรในตำแหน่งนั้นๆได้ เช่น

```go
package main

import "fmt"

func main() {
	names := []string{
		"Weerasak",
		"Kanokorn",
	}

	for _, name := range names {
		fmt.Println(name)
	}
}
```

ส่วนถ้าเราอยากได้แค่ index เราสามารถ ใช้แค่ตัวแปรเดียวได้เช่น

```go
package main

import "fmt"

func main() {
	names := []string{
		"Weerasak",
		"Kanokorn",
	}

	for i := range names {
		fmt.Println(i, ":", names[i])
	}
}
```

## Slice operator
หลังจากเราใช้ append เพิ่มข้อมูลลงไปใน slice และ ใช้ `[indexNumber]` เพื่อเข้าถึงข้อมูลใน slice แต่ละค่าได้แล้ว ต่อไปเราจะดูวิธีการที่ทำให้เราหั่น slice ให้เล็กลง เช่น

```go
package main

import "fmt"

func main() {
	nums := []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10} // index 0..9
	fmt.Println(nums)

	nums = nums[1:5]

	fmt.Println(nums)
}
```

การใช้ slice operator จะกำหนดค่า index 2 ค่าที่คั่นโดย `:` เช่น `a:b` ซึ่งสมาชิกที่ได้หลังจากหั่นแล้วจะคือสมาชิกในช่วง `a` ถึง `b-1` อยากตัวอย่างด้านบนเรากำหนด `1:5` เราก็ได้ผลลัพธ์เป็น 2,3,4,5 นั่นคือค่าใน index 1, 2, 3, 4 นั่นเอง

ส่วน slice ใหม่ที่เราก็ก็จะมีสมาชิกแค่ 4 ตัวและ index ก็เริ่มใหม่เป็น 0 ถึง 3

เราสามารถละค่าแรกของช่วงที่ต้องการ ได้ถ้าเราต้องการตั้งแค่ 0 เช่น

```go
package main

import "fmt"

func main() {
	nums := []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10} // index 0..9
	fmt.Println(nums)

	nums = nums[0:5]

	fmt.Println(nums)
}
```

หรือสามารถละค่าสุดท้ายได้ถ้าต้องการจนถึงค่าสุดท้ายของ slice เช่น

```go
package main

import "fmt"

func main() {
	nums := []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10} // index 0..9
	fmt.Println(nums)

	nums = nums[5:]

	fmt.Println(nums)
}
```

## ใช้ make สร้าง slice

จากก่อนหน้านี้เราสร้าง slice ได้โดยประกาศตัวแปร แล้วใช้ append เพิ่มเข้าไป หรือ สร้าง slice แล้ว กำหนดค่าเริ่มต้นเอาไว้

นอกจากนั้น เรายังสามารถใช้ฟังก์ชัน make สร้าง slice ได้อีกด้วยเช่น

```go
package main

import "fmt"

func main() {
	nums := make([]int, 0) // เทียบเท่ากับ nums := []int{}
	fmt.Println(nums)
}
```

ซึ่ง make ค่าแรกเป็น type ของ slice ที่เราต้องการสร้าง ส่วนค่าที่ 2 เป็นจำนวนสมาชิกเริ่มต้นของ slice ทีนี้ถ้าจำนวนสมาชิกเริ่มต้นไม่เท่ากับ 0 เช่น

```go
package main

import "fmt"

func main() {
	nums := make([]int, 10) // เทียบเท่ากับ nums := []int{0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
	fmt.Println(nums)
}
```

ซึ่งถ้าเรากำหนดจำนวนสมาชิกเริ่มต้นให้ make ก็จะทำให้ได้ slice ที่มีสมาชิกตามนั้นและค่าเริ่มต้นของแต่ละสมาชิกเท่ากับค่า zero value ของ type นั้น เช่น int ได้ 0

## Map

map เป็นข้อมูลที่เก็บเป็น Key : Value คือแทนที่เราจะเก็บเป็นลำดับไปเรื่อยๆแบบ slice แต่เราจะจับคู่สองสิ่งคือ key กับ value ซึ่งข้อมูลใน map key ต้องไม่ซ้ำกัน เช่น

```go
package main

import "fmt"

func main() {
	var scores map[string]int // ค่าเป็น nil

	// scores["John"] = 100 // add key, value to nil will panic
	fmt.Println(scores)

}
```

จากโค้ดนี้เราประกาศตัวแปร scores เป็น type map[string]int จะเห็นว่า type map เราจะใช้คำว่า map แล้วภายใน [] จะเป็น type ของ key ส่วนถ้าหลังสุดเป็น type ของ value

ซึ่งถ้าเราประกาศตัวแปรเฉยๆ ก็จะได้ค่า nil ตอนนี้เราจะข้ามรายละเอียดพิเศษของ nil จำไว้ว่า เราจะเพิ่มค่าเข้าไปใน scores ตอนนี้เลยไม่ได้จนกว่าเราจะสร้างค่า map ด้วยคำสั่ง make หรือใช้การกำหนดค่าเริ่มต้น map ดังตัวอย่างต่อไปนี้

```go
package main

import "fmt"

func main() {
	// scores := make(map[string]int)
	// scores["John"] = 100

	scores := map[string]int {
		"John": 100,
	}

	fmt.Println(scores)
}
```

เราเพิ่มค่าใหม่ใน map โดยใช้ รูปแบบ `varMap[Key] = Value`

และเราเอาค่าออกจาก map โดยใช้ key แบบนี้ `varMap[Key]`

แล้วถ้าเกิด ตัวแปร map ไม่มีค่าที่เป็น key ที่เราต้องการ `varMap[Key]` จะคืนค่า zero value ของ type ที่เป็น value ออกมาเช่น

```go
package main

import "fmt"

func main() {
	// scores := make(map[string]int)
	// scores["John"] = 100

	scores := map[string]int {
		"John": 100,
	}

	fmt.Println(scores["Jan"]) // 0
}
```

เราเลยไม่อาจจะบอกได้ว่าจริงๆแล้วมี key "Jan" ค่า 0 หรือว่าไม่มี key "Jan" อยู่กันแน่ ดังนั้นวิธีการเช็คว่า map ของเรามี key ที่เราต้องการอยู่หรือไม่จะให้วิธีแบบนี้

```go
package main

import "fmt"

func main() {
	scores := map[string]int {
		"John": 100,
	}

	janScore, ok := scores["Jan"]
	if ok { // Found
		fmt.Println("Jan:", janScore)
	} else {
		fmt.Println("Not Found Jan account")
	}
}
```


เราสามารถวนซ้ำไปในแต่ละค่าของ map ได้เช่นเดียวกันกับ slice โดยใช้ for range เช่น

```go
package main

import "fmt"

func main() {
	scores := map[string]int {
		"John": 100,
		"Jan": 80,
	}

	for k, v := range scores {
		fmt.Println(k, ":", v)
	}
}
```

เราสามารถเอาค่าออกจาก map ได้โดยใช้ function delete เช่น

```go
package main

import "fmt"

func main() {
	scores := map[string]int {
		"John": 100,
		"Jan": 80,
	}

	for k, v := range scores {
		fmt.Println(k, ":", v)
	}

	delete(scores, "Jan")

	for k, v := range scores {
		fmt.Println(k, ":", v)
	}
}
```

## Struct

map ทำให้เราเก็บข้อมูลแบบ key value ได้ก็จริงแต่ค่า key ต้องเป็น type เดียวกัน และ value ก็ต้องเป็ type เดียวกัน ในการจัดการข้อมูลจริงๆ เราต้องการมากกว่านี้คือ ต้องการข้อมูลในลักษณะ record ที่มีหลายรค่า แต่ละค่าเป็นข้อมูลคนละแบบ เช่น record ของลูกค้ามี ชื่อ นามสกุล ที่อยู่ อายุ วันเดือนปีเกิด ซึ่งแต่ละ field เก็บข้อมูลกันคนละแบบ สำหรับ Go แล้ว type ที่ทำให้เราเก็บข้อมูลลักษณะนี้ได้คือ struct ตัวอย่างข้อมูลแบบ struct เช่น

```go
package main

import "fmt"

type customer struct {
	firstName string
	lastName  string
	age       int
}

func main() {
	var cust customer

	fmt.Printf("%#v\n", cust)
}
```

เนื่องจาก struct เราต้องตั้งชื่อ field และ ระบุ type ของแต่ละ field ดังนั้นเพื่อให้ง่ายต่อการเอาไปใช้งาน เราจะใช้ keyword type เพื่อสร้าง type และ ตั้งชื่อ type ใหม่ให้กับ struct ของเราคือ customer แล้วจึงเอาไปสร้างตัวแปร cust ได้

จากผลลัพธ์ที่เห็นจะเห็นว่าค่า firstName และ lastName เป็น string ว่างทั้งคู่ และ age เป็น 0 เพราะ ถ้าเราประกาศตัวแปร เฉยๆ แต่ละ field จะเป็น zero value ของ type นั้นๆ

เราสามารถเข้าถึงค่าของแต่ละ field ได้โดยใช้ ชื่อตัวแปร จุด แล้วก็ชื่อ field เช่น

```go
package main

import "fmt"

type customer struct {
	firstName string
	lastName  string
	age       int
}

func main() {
	var cust customer

	cust.firstName = "John"
	cust.lastName = "Doe"
	cust.age = 38

	fmt.Printf("First Name: %s\n", cust.firstName)
	fmt.Printf("Last Name: %s\n", cust.lastName)
	fmt.Printf("Age: %d\n", cust.age)
}
```

ถ้าเราอยากได้ตัวแปรพร้อมค่าแต่ละ field เราสามารถสร้าง struct value พร้อมกำหนดค่าแต่ละ field เองได้ดังนี้

```go
package main

import "fmt"

type customer struct {
	firstName string
	lastName  string
	age       int
}

func main() {
	cust := customer{
		firstName: "John",
		lastName:  "Doe",
		age:       38,
	}

	fmt.Printf("First Name: %s\n", cust.firstName)
	fmt.Printf("Last Name: %s\n", cust.lastName)
	fmt.Printf("Age: %d\n", cust.age)
}
```

## Function

โปรแกรมเริ่มทำงานที่ func main แต่เราไม่จำเป็นต้องเขียนทุกอย่างลงไปใน func main อย่างเดียวเท่านั้น เราสามารถสร้าง function ใหม่เองเพิ่มได้ เพิ่มทำให้โค้ดเราแยกส่วนที่ซ้ำซ้อน หรือ ซับซ้อน ออกเป็น function ย่อยๆ ทำให้โค้ดหลักอ่านเข้าใจง่าย และ ทำให้เรา reuse โค้ดนี้ได้ง่ายๆอีกด้วย

รูปแบบการสร้าง function ของ Go เป็นดังนี้

```go
func funcName(parameterList) (returnTypeList) {
	// Function Body
}
```

เช่น

```go
package main

import "fmt"

func add(a int, b int) int {
	return a + b
}

func main() {
	fmt.Println("add(10,20) =", add(10, 20))
}
```

```go
package main

import "fmt"

func sayHello(name string) {
	fmt.Println("Hello", name)
}

func main() {
	sayHello("John")
}
```

สำหรับ Go เราสามารถมีค่าที่ return ออกจาก function ได้มากกว่า 2 ค่า เช่น

```go
package main

import "fmt"

func doublePair(x int, y int) (int, int) {
	return x*2, y*2
}

func main() {
	x2, y2 := doublePair(10, 20)
	fmt.Println(x2, y2)
}
```

หลักๆแล้วการ return ได้หลายค่าเราจะเห็นเอามาใช้เกี่ยวกับ ฟังก์ชันที่อาจจะเกิด error ขึ้น ซึ่งจะถูกออกแบบมาให้ return 2 ค่า ค่าแรกเป็นผลลัพธ์เมื่อไม่เกิด error ส่วนค่าที่สองเป็น error ที่เกิดขึ้นถ้าหากมี error เช่น function ในการ เปิดไฟล์ใน Go ใน package os มีตัวอย่างการใช้งานแบบนี้

```go
file, err := os.Open("file.go") // For read access.
if err != nil {
	log.Fatal(err)
}
```

สำหรับคนเรียกใช้ ก็ต้องเอาค่าที่ 2 ซึ่งเป็นค่า error มาเช็คว่าไม่เท่ากับ nil หรือไม่ ถ้าไม่เท่ากับ nil แสดงว่ามี error เกิดขึ้น แล้วเราก็จัดการ error นั้นไป

## Parameter Passing

กลไกการส่งค่าไปให้ function Go เป็นภาษาที่เวลาส่งค่าไปใน function เป็นการ pass by value นั่นคือ copy ค่าที่ส่งไปให้กับตัวแปร parameter ของ fuction เวลาทำงาน

## Passing pointer

เรารู้แล้ว่า Go นั้นเวลาส่งค่าไปให้ parameter ของ function นั่นเป็นแบบ pass by value แต่ทีนี้ ถ้าเราต้องการให้ การทำงานของ function สามารถแก้ไขค่าในหน่วยความจำเดียวกันกับค่าที่เราส่งไป จะทำได้ยังไงบ้าง

สำหรับ Go นั้นเราจะมีตัวแปรอีกประเภท เพื่อมาช่วยตรงนี้นั่นคือ Pointer กลไกการทำงานของ type pointer คือสิ่งที่เก็บอยู่ในตัวแปรประเภทนี้ คือตำแหน่งหน่วยความจำ (address)

เมื่อรู้ตำแหน่งหน่วยความจำ Go สามารถกลับไปแก้ไขค่าที่อยู่ในตำแหน่งนั้นได้

```go
package main

import "fmt"

func main() {
	var a int = 10 // ตัวแปร a มีค่า 10
	var b int = a  // copy ค่า 10 จาก a ไป b แต่ a และ b ต่างอยู่คนละตำแหน่งของหน่วยความจำ
	b++
	fmt.Println(a, b) // จะเห็นว่า b เปลี่ยนแต่ a ไม่เปลี่ยนเพราะอยู่คนละที่

	var pa *int // สร้างตัวแปร pointer ที่ชี้ไปที่ตำแหน่งหน่วยความจำของข้อมูลประเภท int

	pa = &a // ใช้ & ข้างหน้าตัวแปร ทำให้ได้ตำแหน่งหน่วยความจำของ a ออกไปเก็บไว้ใน pa

	// เมื่อเรามี pa ที่เป็น pointer ที่เก็บตำแหน่งหน่วยความจำแล้ว เราสามารถใช้ operator * ข้างหน้าตัวแปร pointer
	// เพื่อทำให้เราจัดการกับค่าที่อยู่ในตำแหน่งนั้นได้เช่น

	*pa = 500
	fmt.Println(a) // จะเห็นว่าค่าในตำแหน่ง a เปลี่ยนไปแล้วเพราะโดน pa ใช้ * มาเปลี่ยนค่าได้
}
```

จากความสามารถแบบนี้ เราสามารถสร้าง function ที่แทนที่จะรับค่าธรรมดา เปลี่ยนเป็นรับค่าแบบ pointer เพื่อจัดการค่าที่อยู่ในหน่วยความจำตำแหน่งอื่นได้ เช่น

```go
package main

import "fmt"

// double รับค่า pointer ของ int แล้วเอาค่าที่ pointer ชี้อยู่มาเพิ่มค่าเป็น เท่า

func double(p *int) {
	*p = *p * 2
}

func main() {
	i := 10

	// เมื่อ double รับ pointer ของ int (*int) เราเลยส่ง i เฉยๆไปไม่ได้ compiler จะฟ้องว่ามันผิด type
	// เราต้องใช้ & ข้างหน้า i เพื่อบอกว่าอยากได้ address ของ i แทนที่ค่าใน i
	double(&i)

	fmt.Println(i) // ค่าใน i เปลี่ยนเป็น 20 เพื่อโดน pointer p ใน double เปลี่ยนค่า
}
```

นอกจากเราจะใช้การส่งผ่านค่า pointer แล้ว ยังมีอีก pattern นึงที่จะใช้ และ เจอใน library ต่างๆบ่อยๆคือ function ที่ return pointer กลับออกมา เช่นที่ติดมากับเป็น built in function เลยคือ function new

```go
package main

import "fmt"

func main() {
	// new จะสร้างพื้นที่ในหน่วยความจำแล้วส่งค่า address กลับออกมาเก็บในตัวแปร pa
	// ซึ่งเทียบเท่ากับเราประกาศตัวแปรเองก่อน var a int
	// แล้วเรียก pa := &a
	// แต่ทำได้โดยไม่ต้องมีตัวแปร a ใช้ new(int) ได้เลย

	pa := new(int)
	*pa = 10
	fmt.Println(*pa)
}
```

แต่ว่า new จะไม่ได้กำหนดค่าเริ่มต้นอะไร เพราะฉนั้นค่าในตำแหน่งที่ pa ชี้อยู่จึงเป็น 0 ตอน new

เราอาจจะสร้าง func ใหม่เพื่อสร้าง int พร้อมที่กำหนดค่าเริ่มต้นแล้ว return address กลับออกมาแบบนี้

```go
package main

import "fmt"

func pInt(i int) *int {
	return &i
}

func main() {
	pa := pInt(10)
	fmt.Println(*pa)
}
```

## Pointer to struct

ถ้าเรามีตัวแปร pointer ชี้ไปที่ struct เราก็สามารถเปลี่ยนค่าในตำแหน่งนั้นได้เช่นกัน เช่น

```go
package main

import "fmt"

type customer struct {
	firstName string
	lastName  string
	age       int
}

func main() {
	cust := customer{
		firstName: "John",
		lastName:  "Doe",
		age:       38,
	}

	var pc *customer = &cust

	*pc = customer{
		firstName: "Cherpang",
		lastName: "BNK48",
		age: 23,
	}

	fmt.Printf("First Name: %s\n", cust.firstName)
	fmt.Printf("Last Name: %s\n", cust.lastName)
	fmt.Printf("Age: %d\n", cust.age)
}
```

แต่สิ่งที่เพิ่มเข้ามานอกจากตัวแปรแบบอื่นคือ ถ้าเราไม่ได้ต้องการเปลี่ยน struct ใหม่ทั้งก้อน แต่เราต้องการเปลี่ยน field ใด field หนึ่งใน struct นั้น แล้วเรามี pointer ของ struct นั้นอยู่ เราสามารถใช้ dot `.` ตามด้วย field แล้วเปลี่ยนค่าได้เลย


```go
package main

import "fmt"

type customer struct {
	firstName string
	lastName  string
	age       int
}

func main() {
	cust := customer{
		firstName: "John",
		lastName:  "Doe",
		age:       38,
	}

	var pc *customer = &cust

	pc.firstName = "Cherpang"
	pc.age = 24

	fmt.Printf("First Name: %s\n", pc.firstName)
	fmt.Printf("Last Name: %s\n", pc.lastName)
	fmt.Printf("Age: %d\n", pc.age)
}
```

เราสามารถสร้าง struct โดยได้ pointer กลับมาได้เลย แบบนี้

```go
package main

import "fmt"

type customer struct {
	firstName string
	lastName  string
	age       int
}

func main() {
	pc := &customer{
		firstName: "John",
		lastName:  "Doe",
		age:       38,
	}

	fmt.Printf("First Name: %s\n", pc.firstName)
	fmt.Printf("Last Name: %s\n", pc.lastName)
	fmt.Printf("Age: %d\n", pc.age)
}
```

นอกจากนั้นที่จะเจอบ่อยๆคือ function ที่ return pointer ของ struct เช่น

```go
package main

import (
	"errors"
	"fmt"
	"log"
)

type customer struct {
	firstName string
	lastName  string
	age       int
}

func newCustomer(firstName, lastName string, age int) (*customer, error) {
	if age <= 0 {
		return nil, errors.New("customer: cannot new customer with age less than or equal zero")
	}

	return &customer{
		firstName: firstName,
		lastName:  lastName,
		age:       age,
	}, nil
}

func main() {
	cust, err := newCustomer("John", "Doe", 38)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("First Name: %s\n", cust.firstName)
	fmt.Printf("Last Name: %s\n", cust.lastName)
	fmt.Printf("Age: %d\n", cust.age)
}
```


## Passing slice

การส่งค่า slice จริงๆแล้วก็คือการ copy เช่นกันแม้ว่าตอนเราใช้งานจะรู้สึกเหมือนไม่ได้เป็นการ copy ก็ตามเช่น

```go
package main

import "fmt"

func sortInts(nums []int) {
	length := len(nums)
	for i := 0; i < length-1; i++ {
		for j := i + 1; j < length; j++ {
			if nums[j] < nums[i] {
				nums[i], nums[j] = nums[j], nums[i]
			}
		}
	}
}

func main() {
	nums := []int{5, 4, 3, 2, 1}

	sortInts(nums)
	fmt.Println(nums)
}

```

เพราะจริงๆแล้วโครงสร้างด้านใน slice มี pointer ชี้ไปที่ชุดข้อมูลชุดเดียวกันอยู่ทำให้ function สามารถเปลี่ยนข้อมูลภายใน slice ที่ส่งไปได้

แต่ข้อควรระวังคือ ถ้าเกิด slice สองตัวนี้ยังเป็น slice คนละตัวกัน มีโอกาสที่ slice ด้านในของ function เปลี่ยนไปเก็บข้อมูลชุดอื่นแทน แบบนั้นจะไม่มีผลกับ slice ด้านนอก ถ้าต้องการให้มีผมคือ return slice กลับออกมากำหนดค่าให้ slice ตัวเดิม นั่นคือเหตุผลว่าทำไม function append ถึงใช้ในรูปแบบ return slice ใหม่กลับออกมา เพราะ append ทำให้เกิดการเปลี่ยนแปลง pointer ภายใน slice ของ function ที่ไม่ทำให้ตัวนอกเปลี่ยนไปด้วยนั่นเอง

ตัวอย่างเช่น

```go
package main

import (
	"fmt"
	"math/rand"
)

func appendRandom(nums []int) {
	n := rand.Intn(100)
	nums = append(nums, n)
}

func main() {
	nums := []int{5, 4, 3, 2, 1}

	appendRandom(nums)

	fmt.Println(nums)
}
```

เราแก้ไข appendRandom ใหม่ให้ return slice ตัวใหม่ออกมาเล่น

```go
package main

import (
	"fmt"
	"math/rand"
)

func appendRandom(nums []int) []int {
	n := rand.Intn(100)
	return append(nums, n)
}

func main() {
	nums := []int{5, 4, 3, 2, 1}

	nums = appendRandom(nums)

	fmt.Println(nums)
}
```

## Passing map

การส่งค่า map จะมีพฤติกรรมเหมือนกับ pointer เลยคือเมื่อเราส่งเข้าไป function สามารถแก้ไข สิ่งที่อยู่ภายใน map ตัวนั้นได้ เช่น

```go
package main

import "fmt"

func addGrade(grades map[string]string, name string, score int) {
	if score >= 50 {
		grades[name] = "PASS"
	} else {
		grades[name] = "FAIL"
	}
}
func main() {
	grades := map[string]string{
		"John": "PASS",
		"Jan":  "FAIL",
	}

	addGrade(grades, "Music", 100)
	addGrade(grades, "Munich", 49)

	for k, v := range grades {
		fmt.Println(k, ":", v)
	}
}
```

## Function type and value

function ของ Go นอกจากเราจะประกาศแล้วเรียกใช้แบบที่ผ่านมาแล้วนั้น function ยังมี type และ value ซึ่งหมายความว่า
* เราสร้างตัวแปรเก็บ function ได้
* เราส่งผ่าน function ไปเป็น parameter ให้กับ function อื่นได้
* เราสร้าง function ที่ return ค่า function ออกมาได้

กลไกการทำงานของ function แบบนี้ ช่วยให้เราออกแบบโปรแกรมได้ง่ายยิ่งขึ้น เช่น ตัวอย่าง hello world v2 ที่เราเขียนไปจะเห็นว่าเราสามารถ ส่ง function ที่จะถูกเรียกทำงานเมื่อมีการ request ไปที่ path ที่ตรงกับค่าแรกที่ส่งให้ HandleFunc ได้

ตัวอย่างการสร้างตัวแปร เก็บค่าของ function

```go
package main

import "fmt"

func main() {
	printf := fmt.Printf

	printf("Hello, %s\n", "John")
}
```

เราสามารถใช้ `fmt.Printf` ร่วมกับ `%T` เพื่อให้แสดง type ของค่าใดๆ รวมทั้ง function ก็ได้ด้วยเช่นกัน

```go
package main

import "fmt"

func add(a, b int) int {
	return a + b
}

func main() {
	fmt.Printf("%T\n", add) // add : func(int, int) int
}
```

จะเห็นว่าชื่อ type จะมีแค่ func ตามด้วยลิสต์ของ type ของแต่ละ parameters และ ลิสต์ของ type ของ return type (ถ้ามีตัวเดียวมันจะไม่มีวงเล็บ)

## Anonymouse function and closure

เราสามารถสร้าง function ได้อีกแบบโดยไม่ต้อง ประกาศ function พร้อมชื่อแบบที่ผ่านมา เรียกว่า anonymouse function เช่น

```go
package main

import "fmt"

func main() {
	add := func(a, b int) int {
		return a + b
	}
	fmt.Printf("%T\n", add) // add : func(int, int) int
	fmt.Println(add(10, 20))
}
```

ส่วน closure คือการที่ function ที่เราสร้างด้วย anonymouse function สามารถอ้างถึง ค่าที่อยู่ภายใน function ที่ครอบมันอยู่ได้เช่น

```go
package main

import "fmt"

func main() {
	var num int
	inc := func() {
		num++
	}

	inc()
	inc()
	inc()

	fmt.Println(num)
}
```

เราจะได้ใช้ anonymouse บ่อยๆในกรณีที่เราต้องส่งค่าเข้าไปใน function หรือต้องการ return function กลับออกมาเช่น

```go
package main

import "fmt"

func mapInt(nums []int, fn func(int) int) {
	for i, v := range nums {
		nums[i] = fn(v)
	}
}

func composeFn(f, g func(int) int) func(int) int {
	return func(n int) int {
		return f(g(n))
	}
}

func applyInt(nums []int, fn func(int)) {
	for _, v := range nums {
		fn(v)
	}
}

func main() {
	nums := []int{1, 2, 3, 4, 5, 6}

	mapInt(nums, composeFn(func(n int) int {
		return n + 3
	}, func(n int) int {
		return n * 2
	}))

	applyInt(nums, func(n int) {
		fmt.Println(n)
	})
}
```

## Function ที่สามารถมี Error ได้

Go ให้เราจัดการ error โดยอาศัยค่า error ที่เป็น type interface ซึ่งเดี๋ยวเราจะลงรายละเอียดกันอีกครั้ง แต่ว่า standard package `errors` และ `fmt` ก็เตรียมฟังก์ชันที่ช่วยให้เราสร้างค่า error ที่ไม่ซับซ้อนมากให้ใช่งานก่อนได้

ซึ่งการเขียน function ที่อาจจะเกิด error แทนที่จะใช้การ throw exception ออกมา สำหรับ Go ใช้การ return ค่า error ออกมาแทน ทำให้ผู้เรียกใช้ function จัดการค่า error นั้นแทน เช่น

```go
package main

import (
	"errors"
	"fmt"
	"log"
)

func div(x, y float64) (float64, error) {
	if y == 0 {
		return 0, errors.New("div: cannot divide by zero")
	}

	return x / y, nil
}

func main() {
	x, y := 10.0, 0.0

	result, err := div(x, y)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("%f / %f = %f\n", x, y, result)
}
```

## การสร้าง library package

เราได้เรียนรู้เรื่อง ตัวแปร เรื่อง function กันไปแล้ว และได้ลองเรียกใช้ func จาก package อื่นๆเช่น fmt ไปแล้ว แต่ว่าเรายังเขียนโค้ดกันอยู่ใน package main และ function main อย่างเดียว

จริงๆแล้วเวลาระบบเริ่มซับซ้อนเราสามารถแบ่งออกเป็น package หลายๆ package ได้

## Package path vs Package name

package path คือ ชื่อ path ที่เก็บ package นั้นอยู่ ซึ่งสำหรับ standard package นั้นจะถูกเก็บไว้ที่ path ของ `$GOROOT/src` เช่น fmt ที่เราใช้บ่อยๆก็จะอยู่ที่ `$GOROOT/src/fmt`

แต่สำหรับ package ที่เราสร้างขึ้นเอง PATH ของมันจะถูกมองหาจากที่ `$GOPATH/src` เป็นต้นไป เช่น ถ้าเราจะสร้าง package ชื่อ greeting และเก็บไว้ที่ `$GOPATH/src/greeting` เวลาเรา import package นี้มาใช้งานก็เป็น `import "greeting"` หรือถ้าเราเอาไว้ที่ `$GOPATH/src/github.com/myaccount/greeting` เวลา import ก็เป็น `import "github.com/myaccount/greeting"`

ส่วน package name คือชื่อของ package ที่เราประกาศไว้ส่วนต้นๆของไฟล์ source code ที่อยู่ภายใต้ package path นั่นๆเช่น เราจะตั้งชื่อ package เราว่า `greeting` และเก็บโค้ดไว้ที่ไฟล์ `$GOPATH/src/greeting/greeting.go` ในไฟล์ `greeting.go` ก็ต้องมีประกาศ `package greeting` เอาไว้ด้วยที่ต้นๆไฟล์

### ทดลองสร้าง package greeting
* ให้สร้าง directory ที่ `$GOPATH/src/greeting`
* จากนั้นสร้างไฟล์ `greeting.go`
* เพิ่มโค้ดใน `greeting.go` ดังต่อไปนี้

```go
package greeting

import "fmt"

func SayHello(name string) {
	fmt.Println("Hello", name)
}
```

* จากนั้นให้สร้าง package main ที่จะเรียกใช้ greeting ไว้ที่ `$GOPATH/src/hello/main.go` โดยมีโค้ดต่อไปนี้

```go
package main

import "greeting"

func main() {
	greeting.SayHello("KBTG")
}
```

## การ export var, const, func, type, struct field ที่ประกาศอยู่ใน library package

จะเห็นว่าเวลาเรา import package อื่นเขามาเพื่อจะใช้งานนั้นหลักๆเราคือเรียก function ที่อยู่ภายใน package นั้นๆ ซึ่ง สิ่งที่เราจะเรียกใช้งานได้นั้นต้องเป็นสิ่งที่ export ก่อนถึงจะใช้งานได้

สำหรับ Go นั้นไม่ได้มี keyword อะไรในการระบุว่า function นั้น export แล้วหรือไม่ แต่เลือกที่จะใช้ชื่อ ถ้าชื่อขึ้นต้นด้วยอักษรภาษาอังกฤษตัวใหญ่ function นั่นถือว่า export ออกจาก package นั้น สามารถเรียกใช้ได้

ส่วนฟังก์ชันที่เป็นอย่างอื่น โค้ดภายใน package เดียวกันเรียกใช้กันได้เท่านั้น

ลองกลับไปเปรียบ SayHello เป็น sayHello แล้วลอง compile โค้ดที่ main package อีกครั้งจะเห็นว่า compile ไม่ผ่าน

นอกจาก func ที่เราสามารถจำเป็นต้อง export ก่อนแล้วคนใช้งาน package ถึงจะเรียกใช้ได้นั้น ภายใน package ยังสามารถประกาศตัวแปร (var), ค่าคงที่ (const), ประเภทข้อมูล (type) ใหม่ เช่น `type Customer Struct` และชื่อของแต่ละ field ใน struct ก็ต้องเป็นชื่อที่ export แล้วเท่านั้นถึงจะให้คนที่ใช้งาน package จากภายนอกเข้าถึงได้ เช่น

```go
package greeting

const (
	TH = "Thai"
	EN = "English"
)

var Lang = EN

type Request struct {
	Name string
}

type Response struct {
	Msg string
}

func SayHello(req Request) Response {
	switch Lang {
	case TH:
		fmt.Println("สวัสดี", req.Name)
	case EN:
		fmt.Println("Hello", req.Name)
	}
}
```

## Custom type

เราสามารถสร้าง type ใหม่ โดยอาศัยโครงสร้างข้อมูลเดิม แต่ว่าตั้งชื่อให้ใหม่ได้ เช่นแบบที่เราทำมาแล้วกับ struct คือใช้ keyword คำว่า type

แต่ไม่ใช่แค่ struct เท่านั้นที่เอามาใช้สร้าง type ใหม่ได้ เราสามารถใช้ type ที่มีอยู่เดิม type ไหนก็ได้มาสร้าง type ใหม่เช่น

```go
package main

import "fmt"

type MyInt int

func main() {
	var mi MyInt = 10

	fmt.Printf("%T, %d\n", mi, mi) // main.MyInt, 10
}
```

จะเห็นว่าชื่อ type เปลี่ยนเป็น `main.MyInt` แต่ค่าเรายังสามารถใช้ค่าจาก integer เก็บได้เช่นเดิม

ทำไมต้องตั้งชื่อ type ใหม่ เหตุผลแรกคือเรื่องชื่อนั่นเอง บางครั้งเรารู้ว่าข้อมูลเป็นจำนวนเต็มนี่แหละ หรือแม้แต่ string แต่ว่า ตัวเลขมันเป็นเลขของอะไรล่ะ นอกจากชื่อตัวแปร ที่สื่อความหมาย การที่เราสามารถสร้าง type ใหม่พร้อมชื่อที่สื่อความหมายก็ทำให้โค้ดอ่านเข้าใจง่ายขึ้นเช่นกัน เช่น

```go
package main

import "fmt"

type Address string

func main() {
	var addr Address = "123 Bangkok Thailand"

	fmt.Println(addr)
}
```

ใน library package หลายๆ package ก็ใช้การสร้าง type ใหม่ขึ้นมาเช่นกันตัวอย่างเช่น package time ที่ใช้ในการจัดการเรื่อง date/time นั้นมีฟังก์ชัน Sleep เพื่อใช้หยุดการทำงานของโปรแกรมชั่วคราวตรงจุดที่เรียก function นี้โดยให้เราใส่จำนวนเวลาที่ต้องการหยุด ซึ่ง ถ้าเราดู type ที่ Sleep รับค่าจะเป็นแบบนี้

```go
func Sleep(d Duration)
```

ซึ่ง Duration เป็น type ที่ประกาศไว้แบบนี้

```go
type Duration int64
```

นอกจากนั้น package time ยังมีค่า `const` ที่ประกาศไว้เป็น type Duration อีกชุดนึงแบบนี้

```go
const (
	Nanosecond  Duration = 1
	Microsecond          = 1000 * Nanosecond
	Millisecond          = 1000 * Microsecond
	Second               = 1000 * Millisecond
	Minute               = 60 * Second
	Hour                 = 60 * Minute
)
```

ตอนใช้งาน time.Sleep เลยสามารถใช้แบบนี้ได้

```go
time.Sleep(5 * time.Hour)
```

## การ convert ค่าของ custom type

การสร้าง type ใหม่ แม้ว่าโครงสร้างการเก็บข้อมูลจะเหมือน type เดิม แต่ว่า Go ถือว่าเป็น type คนละ type ไปแล้ว ตัวแปรคนละ type ไม่สามารถกำหนดค่าให้กับตัวแปรอื่นได้ เช่น

```go
package main

import "fmt"

type MyInt int

func main() {
	var i int = 10
	var mi MyInt = i // compile error => cannot use i (type int) as type MyInt in assignment

	fmt.Println(mi)
}
```

เราต้องแปลงข้อมูลก่อน ซึ่งวิธีการแปลง ระหว่างตัวแปรของ type ใหม่ กับ type ที่เป็นโครงสร้างของมัน เราแค่เอาชื่อ type ด้านหน้าแล้วเอาวงเล็บครอบค่าที่ต้องการแปลงแบบนี้

```go
package main

import "fmt"

type MyInt int

func main() {
	var i int = 10
	var mi MyInt = MyInt(i)

	fmt.Println(mi)

	var mj MyInt = 20
	var j int = int(mj)

	fmt.Println(j)
}
```

## OOP with Go Method

Go เป็น OOP ในเรื่อง concept ที่บอกว่า พฤติกรรา (behavior) object นั้นแสดงผ่านทาง method และ การทำงานของโปรแกรมนั่นเกิดจากการที่ object แต่ละตัวส่ง message (เรียก method) ไปหา object อื่น

แต่ Go ไม่มี concept เรื่อง inheritance ที่ให้เรา extend behavior จาก class / type อื่น

สิ่งที่ต่างอีกอย่างกับภาษาที่มี concept OOP ส่วนใหญ่คือ Go เรื่องที่จะไม่มี class แต่ทุก type สามารถมี method ได้ ตัวอย่างการสร้าง method เช่น

```go
package main

import "fmt"

type MyInt int

func (i MyInt) add(n MyInt) MyInt {
	return i + n
}

func main() {
	i := MyInt(10)
	i = i.add(20)

	fmt.Println(i)
}
```

จากตัวอย่าง เราก็ใช้ keyword func ในการสร้าง method เช่นเดียวกันกับตอนสร้าง func เพราะกลไลต่างๆแทบไม่ต่างกัน สิ่งที่เพิ่มเข้ามาคือมีตัวแปร ด้านหน้าชื่อ function เราเรียกตัวแปรนี้ว่าตัวแปร receiver

ตัวแปร receiver ทำหน้าที่สองอย่างหลักๆ อยากแรกเลยคือ type ของมันจะเป็นตัวบอกว่า method นี้เป็น method ของอะไร อย่างเช่น Add นี้เป็น method ของ MyInt

หน้าที่ต่อไปของตัวแปรนี้คือเวลาที่เราเรียกใช้ method จะเห็นว่าเมื่อเรามีตัวแปรของ type ที่มี method เช่น ตัวแปร i type MyInt เราสามารถเรียก method นั้นได้โดยใช้ จุด `.` แล้วตามด้วยชื่อ method ตามด้วย parameter เหมือนเรียก function เวลา method ทำงาน ค่าของตัวแปรที่เป็นคนเรียก method นั้น (ค่าที่อยู่ด้านซ้ายของ `.`) จะถูกส่งผ่านไปหาตัวแปร receiver แบบเดียวกันการส่งค่าไปให้ parameter ของฟังก์ชันนั่นเอง

อีกตัวอย่างเป็น method ของ type struct บ้าง

```go
package main

import (
	"fmt"
)

type Customer struct {
	FirstName string
	LastName string
}

func (c Customer) Name() string {
	return fmt.Sprintf("%s %s", c.FirstName, c.LastName)
}

func main() {
	c := Customer {
		FirstName: "John",
		LastName: "Doe",
	}

	fmt.Println(c.Name())
}
```

## Value Receiver and Pointer Receiver

เราสามารถประกาศตัวแปรของ receiver เป็น pointer ได้ ซึ่งการส่งค่าของ object ที่เรียก method ไปให้ receiver ก็เหมือนกันกับการส่งค่า parameter ที่เป็น pointer คือต้องส่ง address ไป

ส่งที่ต่างคือเราไม่จำเป็นต้องใช้ `&` หน้าตัวแปรเพราะ เวลาเรียก method ด้วย `.` Go จะรู้เองว่าถ้า method นั้นมี receiver เป็น type pointer จะเลือกส่ง address ไปให้เอง

และแน่นอนเมื่อเราส่ง address และ receiver เป็น pointer การทำงานใน method ก็สามารถแก้ไขค่าของ object ที่เป็นคนเรียก method นี้ได้เช่นกัน ตัวอย่างเช่น

```go
package main

import (
	"fmt"
)

type MyInt int

func (i *MyInt) Inc() {
	*i += 1 // ใช้ += แทน *i = *i + 1
}

func (i *MyInt) Double() {
	*i *= 2 // ใช้ *= แทน *i = *i * 2
}

func main() {
	i := MyInt(10)

	i.Inc()
	i.Double()

	fmt.Println(i)
}
```

ในทางกลับกัน ถ้าเรามีตัวแปรที่เป็น pointer เราก็สามารถเรียกใช้ method ที่รับ receiver เป็น type ที่ไม่เป็น pointer ได้เช่นกันเพราะเวลาเราเรียก มันจะส่งค่าที่ pointer นั้นชี้อยู่ไปแทนที่จะส่ง address อัตโนมัติ ไม่ต้องเขียน `(*pObj).` แบบนี้เอง เช่น

```go
package main

import (
	"fmt"
)

type Customer struct {
	FirstName string
	LastName string
}

func (c Customer) Name() string {
	return fmt.Sprintf("%s %s", c.FirstName, c.LastName)
}

func main() {
	c := &Customer {
		FirstName: "John",
		LastName: "Doe",
	}

	fmt.Println(c.Name())
}
```

## Embedded type

Go Embedded type คือการที่เราสามารถสร้าง struct field โดยที่ใช้แค่ชื่อ type ซึ่ง Go จะมองว่าชื่อ field เป็นชื่อเดียวกันกับชื่อ type นั่นเลย

จุดสำคัญของ Embedded type คือ เราสามารถเรียกใช้งาน field ของ type ที่เรา embed เอาไว้โดยผ่านค่าของ struct ที่ทำหน้าที่ครอบ embed type นั้นเอาไว้ได้เลย ไม่จำเป็นต้อง `.` เข้าถึง field ก่อนสองรอบเช่น

```go
package main

import (
	"fmt"
)

type Name struct {
	FirstName string
	LastName  string
}

type Customer struct {
	Name
	Age int
}

func main() {
	c := Customer{
		Name: Name{
			FirstName: "Weerasak",
			LastName:  "Chongnguluam",
		},
		Age: 33,
	}
	// จุดสำคัญคือตอนสร้างด้วย literal struct ต้องกำหนด field ย่อย ไม่สามารถทำแบบนีได้เลย
	/*
	c := Customer{
		FirstName: "Weerasak",
		LastName:  "Chongnguluam",
		Age: 33,
	}
	*/

	// เวลาเรียกใช้ field ของ Name ที่ถูก embed ใน Customer สามารถเรียกผ่านค่าของ Customer ได้เลย
	// ไม่จำเป็นต้องใช้ c.Name.FirstName หรือ c.Name.LastName

	fmt.Println(c.FirstName, c.LastName)
	c.FirstName = "Jone"
	c.LastName = "Doe"
	fmt.Println(c.FirstName, c.LastName)
}
```

ไม่ใช่แค่ field ย่อย แต่รวมถึง method ของ type ที่โดน embed ไว้ด้วยเช่นกันก็เรียกผ่าน type ที่ครอบอยู่ได้เช่น

```go
package main

import (
	"fmt"
)

type Name struct {
	FirstName string
	LastName  string
}

func (n Name) FullName() string {
	return fmt.Sprintf("%s %s", n.FirstName, n.LastName)
}

type Customer struct {
	Name
	Age int
}

func main() {
	c := Customer{
		Name: Name{
			FirstName: "Weerasak",
			LastName:  "Chongnguluam",
		},
		Age: 33,
	}
	fmt.Println(c.FullName())
	c.FirstName = "Jone"
	c.LastName = "Doe"
	fmt.Println(c.FullName())
}
```

## OOP with Go Interface

* Interface เป็น type ที่ช่วยให้เราทำ Polymorphism ใน Go ได้
* Polymorphism สำหรับ OOP คือการที่ เรามีตัวแปร ที่เรากำหนดค่าของ type อื่นให้มันได้หลาย type

เราจะสร้างตัวแปร interface เราต้องมี type interface ก่อน type interface คือ type ที่มีแค่ลิสต์ของ header ของ method เช่น

```go
package main

type Sender interface {
	Send(msg string) error
}

func main() {
	// ตัวแปร sender มี type Sender ซึ่งเป็น interface
	var sender Sender

	fmt.Println(sender)
}
```

โค้ดด้านบนเราสร้างตัวแปร sender ที่เป็น interface type Sender ซึ่งเมื่อเรายังไม่กำหนดค่าอะไรให้กับมัน มันจะมีค่าเป็น nil

ค่าที่เราจะกำหนดให้กับตัวแปร interface ได้ต้องเป็นค่าของ type ที่ implement interface นั้นอยู่ เช่น ค่าที่จะกำหนดให้กับ sender ได้ต้องเป็นค่าของ type ที่ implement interface Sender

การที่จะ implement interface Sender ง่ายๆเลยคือ type นั้นต้องมี method `Send(msg string) error` นั่นเอง เช่น

```go
package main

import "fmt"

type Sender interface {
	Send(msg string) error
}

type SMS struct{}

func (sms *SMS) Send(msg string) error {
	fmt.Printf("message: %s :Sending...\n", msg)
	return nil
}

func main() {
	// ตัวแปร sender มี type Sender ซึ่งเป็น interface
	var sender Sender
	sender = &SMS{}

	sender.Send("มีเงินเข้าบัญชี 1000.0 บาท")
}
```

จะเห็นว่า การ implement interface ของ Go นั้นไม่ต้องเป็นต้องระบุตรงๆว่าต้องการ implement interface ไหน ชื่ออะไร ขอแค่มี method set ตรงกับที่ลิสต์อยู่ใน type interface ก็ถือว่า implment interface นั้นแล้ว

## interface สำคัญๆ ใน standard package
### interface fmt.Stringer
ใน package `fmt` นั้น function `Println` และ `Print` เราสามารถเปลี่ยนการแสดงผล default ที่มันแสดงค่าของ type เราใหม่ได้โดยใช้ การ implment interface `fmt.Stringer` ซึ่ง method ที่ลิสต์ไว้ใน interface นี้เป็นแบบนี้

```go
type Stringer interface {
        String() string
}
```

นั่นคือถ้า type เรา implement method `String() string` แล้วเราเรียกใช้ fmt.Println โดยส่งค่ามันไปให้ มันจะมาเรียก `String() string` ก่อนเพื่อเอา string ที่ได้ไปแสดง เช่น

```go
package main

import (
	"fmt"
)

// Animal has a Name and an Age to represent an animal.
type Animal struct {
	Name string
	Age  uint
}

// String makes Animal satisfy the Stringer interface.
func (a Animal) String() string {
	return fmt.Sprintf("%v (%d)", a.Name, a.Age)
}

func main() {
	a := Animal{
		Name: "Gopher",
		Age:  2,
	}
	fmt.Println(a)
}
```

### interface error
interface `error` ไม่ได้อยู่ใน package เพราะเป็น type ที่ builtin มาซึ่งมีลิสต์ของ method ดังนี้

```go
type error interface {
        Error() string
}
```

เพราะฉะนั้นจากที่เรา ถ้าเราอยาก error value ที่สร้างขึ้นมาเอง ก็คือสร้าง type ที่ implement method นี้ เช่น

```go
package main

import (
	"fmt"
	"log"
)

type DivByZero float64

func (x DivByZero) Error() string {
	return fmt.Sprintf("Cannot divid %f by zero", float64(x))
}

func div(x, y float64) (float64, error) {
	if y == 0 {
		return 0, DivByZero(x)
	}

	return x / y, nil
}

func main() {
	x, y := 10.0, 0.0

	result, err := div(x, y)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("%f / %f = %f\n", x, y, result)
}
```

### interface io.Reader
interface Reader กำหนดไว้แค่ method เดียวคือ Read แบบนี้

```go
type Reader interface {
        Read(p []byte) (n int, err error)
}
```

### Example Reader
```go
package main

import (
	"bufio"
	"fmt"
	"io"
)

type LineMsg struct {
	ReceiverAccount string
	Msg             string
	cur             int
}

func (line *LineMsg) Read(b []byte) (n int, err error) {
	msg := []byte(line.Msg)
	l := len(msg)
	for i := range b {
		if line.cur == l {
			err = io.EOF
			return
		}

		b[i] = msg[line.cur]
		line.cur++
		n++
	}

	return
}

func main() {
	reader := &LineMsg{
		Msg: `When Read encounters an error or end-of-file condition after successfully reading n > 0 bytes, it returns the number of bytes read. It may return the (non-nil) error from the same call or return the error (and n == 0) from a subsequent call. An instance of this general case is that a Reader returning a non-zero number of bytes at the end of the input stream may return either err == EOF or err == nil. The next Read should return 0, EOF.
Callers should always process the n > 0 bytes returned before considering the error err. Doing so correctly handles I/O errors that happen after reading some bytes and also both of the allowed EOF behaviors.
Implementations of Read are discouraged from returning a zero byte count with a nil error, except when len(p) == 0. Callers should treat a return of 0 and nil as indicating that nothing happened; in particular it does not indicate EOF.
Implementations must not retain p.`,
	}
	scanner := bufio.NewScanner(reader)
	for lineNO := 1; scanner.Scan(); lineNO++ {
		line := scanner.Text()
		fmt.Printf("%d:%s\n", lineNO, line)
	}
}
```

### interface io.Writer
interface Writer กำหนดไว้แค่ method เดียวคือ Write แบบนี้

```go
type Writer interface {
        Write(p []byte) (n int, err error)
}
```

สอง interface นี้มีจุดที่ใช้เยอะมาก ใน standard package เพราะในเรื่องของ input/output นั้นถ้าเรามอง สิ่งที่มันทำก็คือการ read input และ ก็การ write output

แต่เราไม่จำเป็นต้อง fix ว่า input มาจากไหน และ output มาจากไหน นั่นจึงเป็นเหตุผลว่าทำไมถึงเกิด 2 interfaces นี้ขึ้นมา

ก็เพื่อทำให้ทิศทางการอ่านและเขียน สามารถถูก implement ได้หลากหลายวิธี

### Example Writer

```go
package main

import (
	"fmt"
	"log"
	"os"
)

type SMS struct {
	Number string
}

func (sms *SMS) Write(b []byte) (n int, err error) {
	fmt.Println("Sending sms to", sms.Number)
	return os.Stdout.Write(b)
}

func main() {
	sms := &SMS{Number: "028888888"}
	_, err := fmt.Fprintln(sms, "Hello, World")
	if err != nil {
		log.Fatal(err)
	}
}
```

## Parse CSV Example

```go
package main

import (
	"encoding/csv"
	"fmt"
	"io"
	"log"
	"strings"
)

type User struct {
	FirstName string
	LastName  string
	Username  string
}

func main() {
	var users []User

	in := `first_name,last_name,username
"Rob","Pike",rob
Ken,Thompson,ken
"Robert","Griesemer","gri"
`
	r := csv.NewReader(strings.NewReader(in))

	_, err := r.Read()
	if err == io.EOF {
		return
	}
	if err != nil {
		log.Fatal(err)
	}

	for {
		record, err := r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal(err)
		}

		users = append(users, User{
			FirstName: record[0],
			LastName:  record[1],
			Username:  record[2],
		})
	}

	fmt.Println(users)
}
```

## แนวคิด Concurrency
Programming as the composition of independently executing processes.

Go เตรียมกลไกสำหรับการทำสร้างโปรแกรมแบบ concurrency มาให้แล้วได้แก่
* goroutine (concurrent execution)
* channel (synchronization and messaging)
* multi-way concurrent control (select)

## การสร้าง goroutine
ปกติการทำงานของ func ใน Go นั้นจะยังไม่เป็นการทำงานที่แยกออกอิสระจากกันเพราะ เวลาเราเรียกใช้ ต้องรอให้การทำงานเสร็จก่อนถึงจะไปทำคำสั่งถัดไป แต่ goroutine ช่วยให้เราเรียกใช้ func โดยแยกออกไปทำอิสระต่างหาก

Go เรียกแต่ละการทำงานที่แยกอิสระจากกันว่าเป็น 1 goroutine ซึ่งปกติเราสั่งรันโปรแกรมจะเกิด main goroutine เกิดขึ้น

ทีนี้ถ้าเราอยากให้การทำงานของ func เราแยกอิสระไปเป็น goroutine ให้เราจะใช้คำสั่ง go ด้านหน้าการเรียกใช้งานเช่น

```go
package main

import (
	"fmt"
	"time"
)

func say(s string) {
	for i := 0; i < 5; i++ {
		time.Sleep(100 * time.Millisecond)
		fmt.Println(s)
	}
}

func main() {
	// go say("world")
	say("hello")
}
```

## การใช้งาน channel
goroutine ที่แยกออกอิสระออกจากกัน เราสามารถให้มัน sync กานทำงานกันผ่านทาง channel ตัวอย่าง goroutine ที่ส่งข้อมูลหากันผ่าน channel เช่น

```go
package main

import "fmt"

func sum(s []int, c chan int) {
	sum := 0
	for _, v := range s {
		sum += v
	}
	c <- sum // send sum to c
}

func main() {
	s := []int{7, 2, 8, -9, 4, 0}

	c := make(chan int)
	go sum(s[:len(s)/2], c)
	go sum(s[len(s)/2:], c)
	x, y := <-c, <-c // receive from c

	fmt.Println(x, y, x+y)
}
```

```go
package main

import (
	"fmt"
	"time"
)

func genInt(begin, end int) chan int {
	out := make(chan int)
	go func() {
		for i := begin; i <= end; i++ {
			time.Sleep(1 * time.Second)
			out <- i
		}
		close(out)
	}()
	return out
}
func main() {
	done := make(chan struct{})
	go func() {
		for v := range genInt(1, 100) {
			fmt.Println(v)
		}
		done <- struct{}{}
	}()
	go func() {
		for v := range genInt(200, 500) {
			fmt.Println(v)
		}
		done <- struct{}{}
	}()
	<-done
	<-done
}
```

## channel แบบ unbuffer

channel มีสองแบบหลักๆคือ unbuffer กับ buffer ซึ่งทั้งสองแบบมีจังหวะการ block การทำงานต่างกัน สำหรับ unbuffer คือถ้าส่งแล้วต้องรอคนรับเสมอ หรือถ้ารับแล้วต้องรอให้มีคนส่งเสมอ

```go
package main

import (
	"fmt"
)

func main() {
	ch := make(chan int) // make unbuffer channel of int
	go func() {
		ch <- 10 // block, waiting for receiver
	}()
	n := <-ch
	fmt.Println(n)
}
```

```go
package main

import (
	"fmt"
)

func main() {
	ch := make(chan int) // make unbuffer channel of int
	go func() {
		n := <-ch
		fmt.Println(n)
	}()
	ch <- 10 // block, waiting for receiver
}
```

## channel แบบ buffer

channel แบบ buffer นึกภาพเหมือน กล่องจดหมายที่จำกัดจำนวนจดหมายที่ฝากในกล่อง ถ้ากล่องว่าง ส่งข้อความไว้แล้วไปได้เลยไม่ต้องรอ

ถ้ากล่องเต็มต้องรอให้กล่องว่างก่อนถึงวางของได้

ส่วนตอนรับ ถ้ากล่องมีจดหมาย เอาไปได้เลย ถ้ากล่องว่า ต้องรอให้มีจดหมายก่อนถึงหยิบไปได้

```go
package main

import "fmt"

func main() {
	ch := make(chan int, 2)
	ch <- 1
	ch <- 2
	// ch <- 3 // block
	fmt.Println(<-ch)
	fmt.Println(<-ch)
	// fmt.Println(<-ch) // block
}
```

## การใช้ range และ close

เราสามารถเอาข้อมูลออกจาก channel ได้โดยใช้ลูป for range ช่วย เช่น

```go
package main

import (
	"fmt"
)

func gen() chan int {
	out := make(chan int)
	go func() {
		for i := 1; i <= 10; i++ {
			out <- i
		}
		close(out)
	}()
	return out
}

func main() {
	out := gen()
	for v := range out {
		fmt.Println(v)
	}
}
```


## การใช้ select statement
`select` ช่วยให้เราจัดการ channel operations ทั้งรับและส่งหลายอันพร้อมกันได้ แล้วเมื่อ operation ไหนไม่ block ก็จะเลือกทำอันนั้นก่อน เช่น

```go
package main

import (
	"fmt"
)

func gen() chan int {
	out := make(chan int)
	go func() {
		for i := 1; i <= 100; i++ {
			out <- i
		}
		close(out)
	}()
	return out
}

func genEven() chan int {
	out := make(chan int)
	go func() {
		for i := 0; i <= 100; i += 2 {
			out <- i
		}
		close(out)
	}()
	return out
}

func main() {
	out := gen()
	outEven := genEven()
	for {
		select {
		case v, ok := <-out:
			if !ok {
				out = nil
			} else {
				fmt.Println(v)
			}
		case v, ok := <-outEven:
			if !ok {
				outEven = nil
			} else {
				fmt.Println("Even:", v)
			}
		}
		if out == nil && outEven == nil {
			break
		}
	}
}
```

## การใช้งาน sync package
บางครั้งการจัดการ การใช้ channel ในการ sync ข้อมูลก็ไม่ได้เหมาะเสมอไป ดังนั้น standard package จึงมี package `sync` เพื่อช่วยให้เราจัดการข้อมูลที่ทำงานร่วมกันระหว่างหลาย goroutine ได้สะดวกขึ้น

### sync.WaitGroup
type `sync.WaitGroup` ใช้ในกรณีที่ต้องต้องการ sync การจบการทำงานของ goroutine เช่นจากโค้ดด้านบนที่เราเคยใช้การกำหนดค่า nil ให้กับตัวแปร channel เราสามารถเปลี่ยนเป็นการใช้ `sync.WaitGroup` ช่วยได้ดังนี้

```go
package main

import (
	"fmt"
	"sync"
)

func gen(wg *sync.WaitGroup) chan int {
	out := make(chan int)
	go func() {
		for i := 1; i <= 100; i++ {
			out <- i
		}
		wg.Done()
	}()
	return out
}

func genEven(wg *sync.WaitGroup) chan int {
	out := make(chan int)
	go func() {
		for i := 0; i <= 200; i += 2 {
			out <- i
		}
		wg.Done()
	}()
	return out
}

func main() {
	var wg sync.WaitGroup
	wg.Add(2)
	out := gen(&wg)
	outEven := genEven(&wg)
	done := make(chan struct{})
	go func() {
		wg.Wait()
		close(done)
	}()
	for {
		select {
		case v := <-out:
			fmt.Println(v)
		case v := <-outEven:
			fmt.Println("Even:", v)
		case <-done:
			return
		}
	}
}
```

```go
package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

func Get(url string) []byte {
	resp, err := http.Get(url)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()
	result, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}
	return result
}

func main() {
	urls := []string{
		"http://httpbin.org/get",
		"http://httpbin.org/image/jpeg",
		"http://httpbin.org/image/png",
		"https://google.com",
		"http://httpbin.org/get",
		"http://httpbin.org/image/jpeg",
		"http://httpbin.org/image/png",
		"https://google.com",
	}
	for _, url := range urls {
		b := Get(url)
		fmt.Printf("%s", b)
	}
}
```

## Chat TCP App

```go
package main

import (
	"bufio"
	"fmt"
	"log"
	"net"
	"sync"
)

type Conns struct {
	mu  sync.Mutex
	Map map[string]net.Conn
}

func (c *Conns) Add(conn net.Conn) {
	c.mu.Lock()
	defer c.mu.Unlock()

	c.Map[conn.RemoteAddr().String()] = conn
}

func (c *Conns) Delete(conn net.Conn) {
	c.mu.Lock()
	defer c.mu.Unlock()

	delete(c.Map, conn.RemoteAddr().String())
}

func (c *Conns) Broadcast(text string) {
	c.mu.Lock()
	defer c.mu.Unlock()

	for _, conn := range c.Map {
		fmt.Fprintln(conn, text)
	}
}

var conns = &Conns{
	Map: make(map[string]net.Conn),
}

func handle(conn net.Conn) {
	defer conn.Close()
	defer conns.Delete(conn)

	conns.Add(conn)
	fmt.Println(conn.RemoteAddr())
	scanner := bufio.NewScanner(conn)
	for scanner.Scan() {
		input := scanner.Text()
		conns.Broadcast(input)
	}
}

func main() {
	l, err := net.Listen("tcp", ":4567")
	if err != nil {
		log.Fatal(err)
	}
	defer l.Close()

	for {
		conn, err := l.Accept()
		if err != nil {
			log.Fatal(err)
		}
		go handle(conn)
	}
}
```

## การทดสอบด้วย testing package

Go เตรียมเครื่องมาในการเขียนโค้ดเพื่อทดสอบโปรแกรมเอาไว้ให้แล้ว ได้แก่คำสั่ง `go test` และ package `testing`

วิธีเขียนโค้ดเพื่อทดสอบโค้ดใน `package` เราจะสร้างไฟล์ที่ชื่อไฟล์ลงท้ายด้วย `_test.go` เช่น ถ้าเรามี package `mymath` แล้วภายในนั้นมี function `Add` แบบนี้

```go
// mymath.go
package mymath

func Add(a, b float64) float64 {
	return a+b
}
```

เราสามารถสร้างไฟล์ขึ้นมาเขียนโค้ดได้โดยลงท้ายด้วย `_test.go` เช่นตั้งชื่อว่า `mymath_test.go`

```go
// mymath_test.go
package mymath
```

ซึ่งเราสามารถประกาศ package ให้กับไฟล์เทสได้สองโหมด ได้แก่โหมดที่เป็น package เดียวกันกับโค้ดที่เราต้องการทดสอบ กับ คนละ package ซึ่งตอนนี้เราจะลองใช้แบบ package เดียวกันก่อน

ส่วนการเขียนโค้ดการทดสอบนั้น เราจะต้องเขียนอยู่ในรูปแบบของฟังก์ชันที่มีชื่อขึ้นต้นด้วย Test แล้วคำถัดไปต้องไม่ใช้ตัวอักษรตัวเล็ก และต้องรับ parameter 1 ตัวที่มี type `*testing.T` ซึ่งชื่อตัวแปร parameter ตาม convention มักจะใช้ชื่อ `t` ตัวอย่างเช่น

```go
// mymath_test.go
package mymath
func TestAdd_10_and_20(t *testing.T) {
	r := Add(10, 20)
	if r != 30 {
		t.Error("Expected 30 but got", r)
	}
}
 ```

เมื่อเรามีโค้ดสำหรับเทสแล้ว เราสามารถสั่ง run test ได้โดยใช้คำสั่ง go test
ซึ่ง ถ้าฟังก์ชันเทสของเรา ไม่ได้เรียก t.Error จะถึงว่าผ่าน แต่ถ้ามีเรียก t.Error จะ fail

## เทคนิค TDD

TDD หรือชื่อเต็มๆคือ Test Driven Development ซึ่งเทคนิคนี้จะให้เรานั้นคิดและเขียนเทสก่อน แล้วค่อยเขียนโค้ด จากนั้นค่อยๆ refactor โค้ด

ข้อดีของ TDD คือ
* ทำให้เราออกแบบ และนึกถึงมุมของคนที่เรียกใช้โค้ดเราก่อน
* โค้ดที่เราเขียนมีการทดสอบแล้วแน่ๆ
* การที่เรา refactor หรือปรับปรุงการทำงาน สามารถรันเทสเพื่อเช็คได้เลยว่ายังคงทำงานถูกต้องหรือไม่

## เทคนิค Test table

โค้ดที่เราทดสอบนั้น จริงๆแล้วเราต้องการทดสอบการทำงานเดิม แต่ว่าแค่เปลี่ยน input เท่านั้น ดังนั้นแทนที่เราจะแยก 1 test ต่อ 1 pattern ของ input เราสามารถใช้ test function เดียวแล้วเก็บข้อมูลใน slice จากนั้นค่อยวนลูปทดสอบได้เช่น

```go
func TestTruncateString(t *testing.T) {
     var flagtests = []struct {
         input  string
         output string
     }{  // input                         output
         {  "abcdefghi",                  "abcdefghi"},
         {  "abcdefghijklmnopqrst",       "abcdefghijklmnopqrst"},
         {  "abcdefghijklmnopqrstuvwxyz", "abcdefghijklmnopqrst"},
     }
     for _, tt := range flagtests {
				 r := Truncate(tt.input, 20)
				 if r != tt.output {
					 t.Errorf("Expect %s got %s", tt.output, r)
				 }
     }
}
```

### Exercise: Stats
- ให้สร้างฟังก์ชันที่หาค่าน้อยสุดใน slice ของตัวเลข
- ให้สร้างฟังก์ชันที่หาค่ามากสุดใน slice ของตัวเลข
- ให้สร้างฟังก์ชันที่หาค่าเฉลี่ยของตัวเลขที่อยู่ใน slice
- สร้างฟังก์ชันเดียวที่ได้ค่าทั้งหมดนั้นกลับออกมาเป็น struct
```go
func Min(nums []float64) float64
func Max(nums []float64) float64
func Average(nums []float64) float64
type Report struct {
	Min float64
	Max float64
	Average float64
}
func MakeReport(nums []float64) Report
```

### Exercise: LCD Digit
สร้างฟังก์ชันที่รับตัวเลขจำนวนเต็มแล้วให้แสดงผลลัพธ์ออกมาเป็นตัวเลขที่เป็น grid ขนาด 3x3 ที่ตัวเลข 0 ถึง 9 แทนด้วยรูปร่างต่อไปนี้ (จุดในตัวอย่างนี้คือ space ใส่มาเพื่อให้เห็นว่าแต่ละตำแหน่งใน 3x3 เป็นอะไร)

```
._.   ...   ._.   ._.   ...   ._.   ._.   ._.   ._.   ._.
|.|   ..|   ._|   ._|   |_|   |_.   |_.   ..|   |_|   |_|
|_|   ..|   |_.   ._|   ..|   ._|   |_|   ..|   |_|   ..|
```

Example: 910

```
._. ... ._.
|_| ..| |.|
..| ..| |_|
```

### Exercises: Remove Duplicates
ให้เขียน function ที่รับ slice ของ int แล้วลบข้อมูลที่ซ้ำกันออก
```go
[]string{"a", "b", "a"} => []string{"a","b"}
func RemoveDuplicate(nums []string) []string
```

### Exercises: account package
สร้าง package ที่จัดการ account ของบัญชีออมทรัพย์
โดย package account ต้องมี function
- `func New(name string) *Account` // สำหรับสร้าง account ใหม่
- `func Save(account *Account)` // สำหรับ save/update account เอาไว้
- `func FindByName(name string) *Account` // ค้นหา account ที่เก็บเอาไว้

ส่วน *Account นั้นให้มี Method สำหรับ ฝาก และ ถอน เงิน และเรียกดู Balance ดังนี้
```go
func (a *Account) Withdraw(amount int)
func (a *Account) Deposit(amount int)
func (a *Account) Balance() int
```
## การเช็ค code coverage
go test สามารถเช็ค code coverage ให้เราได้ว่า เรานั้นทดสอบโค้ดได้ครอบคลุมแค่ไหน โดยใส่ option `cover` ตอนสั่ง go test เช่น

```shell
go test -cover
```

หรือถ้าต้องการดูว่าบรรทัดไหน cover บ้างให้สั่ง go test กับ option `-coverprofile=cover.out` ซึ่งจะได้ไฟล์ cover.out ที่เราต้องใช้ `go tool cover -html=cover.out` เพื่อดูผลลัพธ์อีกครั้งเช่น

```sh
go test -coverprofile=cover.out
go tool cover -html=cover.out
```

## การเขียน Benchmark Test
เราสามารถทดสอบประสิทธิภาพการทำงานได้ด้วย Benchmark Test ซึ่งเป็นเทสที่จะให้ผลลัพธ์ในเรื่องความเร็ว หน่วยความจำที่ใช้ วิธีการเขียน benchmark ให้สร้าง function ที่ชื่อขึ้นต้นด้วย Benchmark และรับค่า parameter type `*testing.B` เช่น

```go
func BenchmarkAdd(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Add(10, 20)
	}
}
```

ซึ่งเวลาเราต้องการรัน Benchmark ที่เราเขียนไว้ให้ใส่ option `-bench=.` ให้กับคำสั่ง go test เช่น

```sh
go test -bench=.
```

ผลลัพธ์การรัน Benchmark จะเป็นแบบนี้

```
goos: darwin
goarch: amd64
pkg: go-training/mymath
BenchmarkAdd-4   	2000000000	         0.34 ns/op
PASS
```

ตัวเลข 2000000000 คือจำนวนรอบที่ถูกทำงาน และ 0.34 ns/op คือฟังก์ชันที่เราทดสอบ ทำงานด้วยความเร็ว 0.34ns ต่อรอบ

ถ้าเราอยากวัดประสิทธิภาพในด้านหน่วยความจำ เราจะสั่ง go test แบบนี้

```sh
go test -bench=. -benchmem
```

ซึ่งผลลัพธ์ที่ได้จะแสดงการใช้หน่วยความจำ และ จองหน่วยความจำ ที่ Heap memory ออกมาเช่น

```
goos: darwin
goarch: amd64
pkg: go-training/mymath
BenchmarkAdd-4   	2000000000	         0.33 ns/op	       0 B/op	       0 allocs/op
PASS
ok  	go-training/mymath	0.703s
```

## การเขียน Example Test

การทดสอบแบบ Example Test นั่นจะทดสอบสิ่งที่แสดงผลลัพธ์ออกไป ซึ่งจริงๆแล้วเราไม่ค่อยเห็นการเขียนโค้ดทดสอบแบบนี้มากนัก แต่ว่า เรามักจะเอา Example Test มาเป็น document ที่สามารถกดรันได้แทน

การเขียน Example Test นั้นเราจะเขียนขึ้นต้นด้วยคำว่า `Example` และไม่ต้องรับ parameter อะไร เช่น

```go
func ExampleAdd() {
	r := Add(10, 20)
	fmt.Println(r)

	// Output: 30
}
```

ซึ่งถ้าในเทสเรามีคำสั่งที่แสดงว่าที่ standard output เช่น `fmt.Println` เราสามารถเขียนผลลัพธ์ที่เราคาดว่าจะต้องแสดงไว้ได้ที่ comment ท้าย function โดยให้ขึ้นต้น comment ด้วยคำว่า `Output:` จากนั้นเป็น text ที่เราคาดว่าจะเจอเมื่อเทสทำงาน

ส่วนการรันเทสนั้นก็ใช้ go test ได้ปกติเช่นเดียวกันกับ Test แบบแรก
