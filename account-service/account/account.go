package account

import (
	"sort"
	"sync"
)

// ### Exercises: account package
// สร้าง package ที่จัดการ account ของบัญชีออมทรัพย์
// โดย package account ต้องมี function
// - `func New(name string) *Account` // สำหรับสร้าง account ใหม่
// - `func Save(account *Account)` // สำหรับ save/update account เอาไว้
// - `func FindByName(name string) *Account` // ค้นหา account ที่เก็บเอาไว้

// ส่วน *Account นั้นให้มี Method สำหรับ ฝาก และ ถอน เงิน และเรียกดู Balance ดังนี้
// ```go
// func (a *Account) Withdraw(amount int)
// func (a *Account) Deposit(amount int)
// func (a *Account) Balance() int
// ```

var (
	mu      sync.Mutex
	idSeq   int
	storage = make(map[int]Account)
)

type Account struct {
	mu sync.Mutex

	ID      int
	Name    string
	balance int
}

func New(name string) *Account {
	return &Account{
		Name: name,
	}
}

func Save(acc *Account) {
	mu.Lock()
	defer mu.Unlock()

	idSeq++
	acc.ID = idSeq
	storage[idSeq] = *acc

}

func FindByName(name string) *Account {
	mu.Lock()
	defer mu.Unlock()

	for _, v := range storage {
		if name == v.Name {
			return &v
		}
	}
	return nil
}

func All() ([]Account, error) {
	mu.Lock()
	defer mu.Unlock()

	accs := make([]Account, 0, len(storage))
	for _, acc := range storage {
		accs = append(accs, acc)
	}
	sort.Slice(accs, func(i, j int) bool { return accs[i].ID < accs[j].ID })
	return accs, nil
}

func (acc *Account) Withdraw(amount int) {
	acc.mu.Lock()
	defer acc.mu.Unlock()

	acc.balance -= amount
}

func (acc *Account) Deposit(amount int) {
	acc.mu.Lock()
	defer acc.mu.Unlock()

	acc.balance += amount
}

func (acc *Account) Balance() int {
	acc.mu.Lock()
	defer acc.mu.Unlock()

	return acc.balance
}
