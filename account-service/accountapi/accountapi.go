package accountapi

import (
	"account-service/account"
	"encoding/json"
	"fmt"
	"net/http"
)

func StartServer() error {
	http.Handle("/accounts/", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		accs, err := account.All()
		if err != nil {
			http.Error(w, "accouts: "+err.Error(), http.StatusInternalServerError)
			return
		}

		b, err := json.Marshal(accs)
		if err != nil {
			http.Error(w, "accouts: "+err.Error(), http.StatusInternalServerError)
			return
		}

		fmt.Fprintf(w, "%s", b)
	}))
	return http.ListenAndServe(":8000", nil)
}
