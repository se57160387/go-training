package main

import (
	"account-service/accountapi"
	"log"
)

func main() {
	log.Fatal(accountapi.StartServer())
}
