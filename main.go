package main

import "fmt"

func main() {
	a := [][]float64{{1, 2}, {3, 4}}
	b := [][]float64{{5, 6}, {7, 8}}
	fmt.Println(SumMatrix(a, b))
}
func SumMatrix(a, b [][]float64) [][]float64 {
	var c [][]float64
	for i := range a {
		var sumRow []float64
		for j := range a[i] {
			sumRow = append(sumRow, a[i][j]+b[i][j])
		}
		c = append(c, sumRow)
	}
	return c
}
