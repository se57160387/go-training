package main

import (
	"fmt"
	"log"
	"todoapp/todo"
)

func main() {
	t := todo.New("ไปเที่ยว")
	todo.Save(t)
	t2 := todo.New("กลับบ้าน")
	todo.Save(t2)

	for _, v := range todo.List() {
		fmt.Println("ID:", v.ID, "BODY:", v.Body)
	}

	t, err := todo.FindByID(2)
	if err != nil {
		log.Fatal(err)
	}
}
