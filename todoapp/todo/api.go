package todo

import (
	"errors"
	"sort"
)

var todoStorage = make(map[int]Todo)

// Save todo to storage
func Save(t *Todo) error {
	todoStorage[t.ID] = *t
	return nil
}

// FindByID from storage
func FindByID(id int) (*Todo, error) {
	todo, ok := todoStorage[id]
	if !ok {
		return nil, errors.New("todo: not found")
	}
	return &todo, nil
}

// List all from storage ordered by ID
func List() []Todo {
	var todos []Todo
	for _, v := range todoStorage {
		todos = append(todos, v)
	}
	sort.Slice(todos, func(i, j int) bool {
		return todos[i].ID > todos[j].ID
	})
	return todos
}
