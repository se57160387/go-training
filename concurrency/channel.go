package main

import "fmt"

func genFizzBuzz(begin, end int) chan string {
	out := make(chan string)
	go func() {
		for i := begin; i <= end; i++ {
			switch {
			case i%15 == 0:
				out <- "FizzBuzz"
			case i%3 == 0:
				out <- "Fizz"
			case i%5 == 0:
				out <- "Buzz"
			default:
				out <- fmt.Sprint(i)
			}
		}
		close(out)
	}()
	return out
}

func genEven(begin, end int) chan int {
	out := make(chan int)
	go func() {
		for v := range genInt(begin, end) {
			if v%2 == 0 {
				out <- v
			}
		}
		close(out)
	}()
	return out
}

func genOdd(begin, end int) chan int {
	out := make(chan int)
	go func() {
		for v := range genInt(begin, end) {
			if v%2 == 1 {
				out <- v
			}
		}
		close(out)
	}()
	return out
}

func genInt(begin, end int) chan int {
	out := make(chan int)
	go func() {
		for i := begin; i <= end; i++ {
			out <- i
		}
		close(out)
	}()
	return out
}
func main() {
	for v := range genFizzBuzz(1, 15) {
		fmt.Println(v)
	}
	// done := make(chan struct{})
	// go func() {
	// 	for v := range genInt(1, 100) {
	// 		fmt.Println(v)
	// 	}
	// 	done <- struct{}{}
	// }()
	// go func() {
	// 	for v := range genInt(200, 500) {
	// 		fmt.Println(v)
	// 	}
	// 	done <- struct{}{}
	// }()
	// <-done
	// <-done
}
