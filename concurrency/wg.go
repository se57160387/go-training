package main

import (
	"fmt"
	"sync"
)

func Do(wg *sync.WaitGroup) {
	fmt.Println("Done")
	wg.Done()
}

func main() {
	var wg sync.WaitGroup
	wg.Add(2)
	go Do(&wg)
	go Do(&wg)
	wg.Wait()
	fmt.Println("Exit")
}
