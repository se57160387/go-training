package main

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"strconv"
)

func handleAdd(w http.ResponseWriter, r *http.Request) {
	x := r.FormValue("x")
	xnum, err := strconv.ParseFloat(x, 64)
	if err != nil {
		fmt.Fprintln(w, "x is not number")
		return
	}
	y := r.FormValue("y")
	ynum, err := strconv.ParseFloat(y, 64)
	if err != nil {
		fmt.Fprintln(w, "y is not number")
		return
	}
	fmt.Fprintf(w, "<h1>%.2f+%.2f = %.2f</h1>", xnum, ynum, xnum+ynum)
}
func handleHello(w http.ResponseWriter, r *http.Request) {
	name := r.URL.Query().Get("name")
	fmt.Fprintf(w, "<h1>Hello %s</h1>", name)
}
func main() {
	http.HandleFunc("/hello", handleHello)
	http.HandleFunc("/add", handleAdd)
	http.Handle("/www/",
		http.StripPrefix("/www/",
			http.FileServer(http.Dir("."))))
	http.HandleFunc("/hijack/", func(w http.ResponseWriter, r *http.Request) {
		defer r.Body.Close()
		io.Copy(os.Stdout, r.Body)
	})
	http.ListenAndServe(":8000", nil)
}
